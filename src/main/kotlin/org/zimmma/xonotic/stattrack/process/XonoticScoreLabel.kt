package org.zimmma.xonotic.stattrack.process

/**
 * Class representing one score row.
 *
 * @author Marek Zimmermann
 */
data class XonoticScoreLabel(
        var score: Int = 0,
        var caps: Int = 0,

        var captime: Int = 0,
        var drops: Int = 0,
        var fckills: Int = 0,
        var pickups: Int = 0,
        var returns: Int = 0,
        var deaths: Int = 0,
        var dmg: Float = 0.0f,
        var dmgtaken: Float = 0.0f,
        
        
        var elo: Float = 0.0f,
        
        var fps: Int = 0,
        
        
        
        
        
        
        
        
        
        
        
        
        var kills: Int = 0,
        
        
        
        
        
        
        
        
        
        
        
        
        
        var suicides: Int = 0,
        
        var teamkills: Int = 0,
        var playtime: Int = 0,
        var team: String = "", // can also be "spectator"
        var playerId: Int = 0,
        var nicknameFull: String = "",
        var nicknameTextOnly: String = ""
) {
}