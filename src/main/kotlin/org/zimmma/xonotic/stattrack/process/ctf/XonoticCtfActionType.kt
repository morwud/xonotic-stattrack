package org.zimmma.xonotic.stattrack.process.ctf

/**
 * Type of CTF action.
 *
 * @author Marek Zimmermann
 */
enum class XonoticCtfActionType(val value: String) {
    UNKNOWN(""), STEAL("steal"), DROPPED("dropped"), PICKUP("pickup"), CAPTURE("capture"), RETURN("return"), RETURNED("returned");

    companion object {
        fun getCtfActionType(value: String): XonoticCtfActionType {
            for (actionType in values()) {
                if(value == actionType.value)
                    return actionType
            }
            
            throw IllegalArgumentException("Unknown CTF action value '$value'")
        }
    }
}