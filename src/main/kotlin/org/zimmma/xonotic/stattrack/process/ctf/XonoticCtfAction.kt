package org.zimmma.xonotic.stattrack.process.ctf

import org.zimmma.xonotic.stattrack.process.XonoticTeamType
import java.time.LocalDateTime

/**
 * Describe one CTF action.
 *
 * @author Marek Zimmermann
 */
data class XonoticCtfAction(
        val actionType: XonoticCtfActionType = XonoticCtfActionType.UNKNOWN,
        val flagColor: XonoticTeamType = XonoticTeamType.UNKNOWN,
        val stealTeam: XonoticTeamType = XonoticTeamType.UNKNOWN,
        val stealerId: Int = 0,
        val time: LocalDateTime? = null
) {

}