package org.zimmma.xonotic.stattrack.process

/**
 * Enum storing all weapons and their values in Xonotic.
 *
 * Weapon IDs are below 10000.
 *
 * @author Marek Zimmermann
 */
enum class XonoticWeapon(val weaponId: Int) {
    UNKNOWN(-1), BLASTER(1), SHOTGUN(2), MACHINE_GUN(3), MORTAR(4), ELECTRO(5), CRYLINK(6), VORTEX(7), HANGAR(8), ROCKET_LAUNCHER(9), PORT_O_LAUNCH(10), VAPORIZER(11), GRAPPLING_HOOK(12), HEAVY_LASER_ASSAULT_CANNON(13), TAG_SEEKER(14);

    companion object {
        fun getWeaponType(fragValue: Int): XonoticWeapon {
            for (weapon in values().reversed()) {
                if (weapon.weaponId == fragValue) {
                    return weapon
                }
            }

            return UNKNOWN
        }
    }
}