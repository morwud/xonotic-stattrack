package org.zimmma.xonotic.stattrack.analyze

/**
 * Contains stats for one map.
 */
data class MapStat(
        val mapName: String,
        val gameTypes: MutableMap<String, Int> = mutableMapOf(),
        val mapKills: MutableList<Double> = mutableListOf(),
        val totalMapKills: MutableList<Int> = mutableListOf(),
        val mapSuicides: MutableList<Double> = mutableListOf(),
        val totalMapSuicides: MutableList<Int> = mutableListOf(),
        val mapAccidents: MutableList<Double> = mutableListOf(),
        val totalMapAccidents: MutableList<Int> = mutableListOf(),
        val mapWinners: MutableMap<String, Int> = mutableMapOf(),
        var played: Int = 0
) {
    fun addPlayed(gameType: String) {
        gameTypes[gameType] = gameTypes[gameType]?.plus(1) ?: 1
        played++
    }
    
    fun addKills(totalKills: Int, totalPlayers: Int) {
        mapKills.add(totalKills.toDouble() / totalPlayers.toDouble())

        totalMapKills.add(totalKills)
    }
    
    fun addSuicides(totalSuicides: Int, totalPlayers: Int) {
        mapSuicides.add(totalSuicides.toDouble() / totalPlayers.toDouble())

        totalMapSuicides.add(totalSuicides)
    }
    
    fun addAccidents(totalAccidents: Int, totalPlayers: Int) {
        mapAccidents.add(totalAccidents.toDouble() / totalPlayers.toDouble())

        totalMapAccidents.add(totalAccidents)
    }
    
    fun addWinner(winner: PlayerStat) {
        mapWinners[winner.nickname] = mapWinners[winner.nickname]?.plus(1) ?: 1
    }
}