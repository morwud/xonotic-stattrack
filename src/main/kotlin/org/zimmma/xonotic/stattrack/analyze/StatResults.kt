package org.zimmma.xonotic.stattrack.analyze

import org.zimmma.xonotic.stattrack.AccidentTypeFilter
import org.zimmma.xonotic.stattrack.EntityTypeFilter
import org.zimmma.xonotic.stattrack.process.ctf.XonoticCtfActionType
import org.zimmma.xonotic.stattrack.round

/**
 * Class representing the analysis result data that can be printed.
 *
 * @author Marek Zimmermann
 */
data class StatResults(
    val players: MutableMap<String, PlayerStat> = mutableMapOf(),
    val maps: MutableMap<String, MapStat> = mutableMapOf()
) {
    companion object {
        private const val DEFAULT_COUNT = 20
    }

    /**
     * Returns list of [count] top absolute killers.
     *
     * Usable type values are "all", "bots", "players" for getting all the kills, only bot kills or only player kills.
     */
    fun getTopKillers(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            Pair(it.nickname, it.getKillCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    /**
     * Returns list of [count] top killers per minute.
     *
     * Usable type values are "all", "bots", "players" for getting all the kills, only bot kills or only player kills.
     */
    fun getTopKillersPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            Pair(it.nickname, it.getKillCount(type).toDouble() / (it.getPlaytimeInSeconds(type).toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopDeadPlayers(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            Pair(it.nickname, it.getDeathCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopDeadPlayersPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            val playtime = when (type) {
                EntityTypeFilter.ALL -> it.totalPlaytimeSeconds
                EntityTypeFilter.BOTS -> it.totalPlaytimeSeconds - it.totalBotlessPlaytimeSeconds
                EntityTypeFilter.PLAYERS -> it.totalBotlessPlaytimeSeconds
            }
            Pair(it.nickname, it.getDeathCount(type).toDouble() / (playtime.toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopAccidents(
        count: Int = DEFAULT_COUNT,
        type: AccidentTypeFilter = AccidentTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            Pair(it.nickname, it.getAccidentsCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopAccidentsPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: AccidentTypeFilter = AccidentTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            Pair(it.nickname, it.getAccidentsCount(type).toDouble() / (it.totalPlaytimeSeconds.toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopDmgGiven(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            val dmg = when (type) {
                EntityTypeFilter.ALL -> it.dmgGiven
                EntityTypeFilter.BOTS -> it.dmgGiven - it.dmgGivenBotless
                EntityTypeFilter.PLAYERS -> it.dmgGivenBotless
            }
            Pair(it.nickname, dmg)
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopDmgGivenPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            val dmg = when (type) {
                EntityTypeFilter.ALL -> it.dmgGiven
                EntityTypeFilter.BOTS -> it.dmgGiven - it.dmgGivenBotless
                EntityTypeFilter.PLAYERS -> it.dmgGivenBotless
            }
            Pair(it.nickname, dmg / (it.getPlaytimeInSeconds(type).toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopDmgTaken(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            val dmg = when (type) {
                EntityTypeFilter.ALL -> it.dmgTaken
                EntityTypeFilter.BOTS -> it.dmgTaken - it.dmgTakenBotless
                EntityTypeFilter.PLAYERS -> it.dmgTakenBotless
            }
            Pair(it.nickname, dmg)
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopDmgTakenPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            val dmg = when (type) {
                EntityTypeFilter.ALL -> it.dmgTaken
                EntityTypeFilter.BOTS -> it.dmgTaken - it.dmgTakenBotless
                EntityTypeFilter.PLAYERS -> it.dmgTakenBotless
            }
            Pair(it.nickname, dmg / (it.getPlaytimeInSeconds(type).toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopFirstbloodPlayers(count: Int = DEFAULT_COUNT, includeBots: Boolean = false): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            Pair(it.nickname, it.getFirstBloodCounts())
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopFirstbloodPlayersPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            Pair(it.nickname, it.getFirstBloodCounts().toDouble() / (it.getPlaytimeInSeconds(type).toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopFirstbloodPlayerVictims(
        count: Int = DEFAULT_COUNT,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            Pair(it.nickname, it.getFirstBloodVictimCounts())
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopFirstbloodPlayerVictimssPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map {
            Pair(
                it.nickname,
                it.getFirstBloodVictimCounts().toDouble() / (it.getPlaytimeInSeconds(type).toDouble() / 60.0)
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopFavouriteGameModePerPlayer(includeBots: Boolean = false): List<Pair<String, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            val maxPlayedValue = ps.gameModePlayed.maxByOrNull { it.value }?.value
            val topGameModes = ps.gameModePlayed.filter { it.value == maxPlayedValue }

            Pair(ps.nickname, topGameModes.toList().sortedBy { it.first }.joinToString { "${it.first} (${it.second})" })
        }
    }

    fun getTopFavouriteMapNamePerPlayer(includeBots: Boolean = false): List<Pair<String, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            val maxPlayedValue = ps.mapNamePlayed.maxByOrNull { it.value }?.value
            val topMapNames = ps.mapNamePlayed.filter { it.value == maxPlayedValue }
            Pair(ps.nickname, topMapNames.toList().sortedBy { it.first }.joinToString { "${it.first} (${it.second})" })
        }.sortedBy { it.first.toLowerCase() }
    }

    fun getTopFavouriteGameModePerMapName(count: Int = DEFAULT_COUNT): List<Pair<String, Int>> {
        val gmMap: MutableMap<String, Int> = mutableMapOf()
        for (mapStat in maps.values) {
            for ((gm, value) in mapStat.gameTypes) {
                gmMap[gm] = gmMap[gm]?.plus(value) ?: value
            }
        }
        return gmMap.toList().sortedByDescending { it.second }.take(count)
    }

    fun getTopFavouriteMapNamePerMapName(
        count: Int = DEFAULT_COUNT,
        inverted: Boolean = false
    ): List<Pair<String, Int>> {
        return if (inverted)
            maps.values.map { ms ->
                Pair("${ms.mapName} (${ms.gameTypes.keys.joinToString()})", ms.played)
            }.sortedBy { it.second }.take(count)
        else
            maps.values.map { ms ->
                Pair("${ms.mapName} (${ms.gameTypes.keys.joinToString()})", ms.played)
            }.sortedByDescending { it.second }.take(count)
    }

    fun getMapWithTopKills(count: Int = DEFAULT_COUNT): List<Pair<String, Double>> {
        return maps.map { entry ->
            Pair(entry.key, entry.value.totalMapKills.sum().toDouble() / entry.value.totalMapKills.size.toDouble())
        }.sortedByDescending { it.second }.take(count)
    }

    fun getMapWithTopKillsPerPlayers(count: Int = DEFAULT_COUNT): List<Pair<String, Double>> {
        return maps.map { entry ->
            Pair(entry.key, entry.value.mapKills.sum() / entry.value.mapKills.size.toDouble())
        }.sortedByDescending { it.second }.take(count)
    }

    fun getMapWithTopSuicides(count: Int = DEFAULT_COUNT): List<Pair<String, Double>> {
        return maps.map { entry ->
            Pair(
                entry.key,
                entry.value.totalMapSuicides.sum().toDouble() / entry.value.totalMapSuicides.size.toDouble()
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getMapWithTopSuicidesPerPlayers(count: Int = DEFAULT_COUNT): List<Pair<String, Double>> {
        return maps.map { entry ->
            Pair(entry.key, entry.value.mapSuicides.sum() / entry.value.mapSuicides.size.toDouble())
        }.sortedByDescending { it.second }.take(count)
    }

    fun getMapWithTopAccidents(count: Int = DEFAULT_COUNT): List<Pair<String, Double>> {
        return maps.map { entry ->
            Pair(
                entry.key,
                entry.value.totalMapAccidents.sum().toDouble() / entry.value.totalMapAccidents.size.toDouble()
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getMapWithTopAccidentsPerPlayers(count: Int = DEFAULT_COUNT): List<Pair<String, Double>> {
        return maps.map { entry ->
            Pair(entry.key, entry.value.mapAccidents.sum() / entry.value.mapAccidents.size.toDouble())
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopMapsPlayedPlayers(count: Int = DEFAULT_COUNT, includeBots: Boolean = false): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getTotalGamesPlayed())
        }.sortedByDescending { it.second }.take(count)
    }

    fun getKillsPerMapsPlayed(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getKillCount(type).toDouble() / ps.getTotalGamesPlayed().toDouble())
        }.sortedByDescending { it.second }.take(count)
    }

    fun getWinnersByMapAbsolute(count: Int = 0): List<Pair<String, String>> {
        val result = maps.map { entry ->
            val mapPlayed = entry.value.mapWinners.values.sum()
            val winners = entry.value.mapWinners.toList().map {
                val winPercentage = ((it.second.toDouble() / mapPlayed.toDouble()) * 100).round(0).toInt()
                Pair(it.first, winPercentage)
            }.sortedByDescending { it.second }

            return@map if (winners.isEmpty())
                Pair("${entry.key} (${mapPlayed})", "NOBODY (0%)")
            else
                Pair("${entry.key} (${mapPlayed})", winners.joinToString { "${it.first} (${it.second}%)" })
        }.sortedBy { it.first }

        return if (count > 0) result.take(count) else result
    }

    fun getWinnersByMapPerPresence(count: Int = 0): List<Pair<String, String>> {
        val result = maps.map { entry ->
            val mapPlayedCount = entry.value.mapWinners.values.sum()
            val winners = entry.value.mapWinners.toList().map {
                val mapPlayed = players[it.first]!!.mapNamePlayed[entry.key] ?: it.second
                val winPercentage = ((it.second.toDouble() / mapPlayed.toDouble()) * 100).round(0).toInt()
                Pair(it.first, winPercentage)
            }.sortedByDescending { it.second }

            return@map if (winners.isEmpty())
                Pair("${entry.key} (${mapPlayedCount})", "NOBODY (0%)")
            else
                Pair("${entry.key} (${mapPlayedCount})", winners.joinToString { "${it.first} (${it.second}%)" })
        }.sortedBy { it.first }

        return if (count > 0) result.take(count) else result
    }

    fun getTotalPlayTime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getPlaytimeInSeconds(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopHeadshotsGivers(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getHeadshotsGiven(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopHeadshotsGiversPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getHeadshotsGiven(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopHeadshotsTakers(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getHeadshotsTaken(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopHeadshotsTakersPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getHeadshotsTaken(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopKillingSpree(
        count: Int = DEFAULT_COUNT,
        includeBots: Boolean = false
    ): List<Triple<String, Int, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Triple(ps.nickname, ps.killingSpree.first, ps.killingSpree.second)
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopStrengthKillingSpree(
        count: Int = DEFAULT_COUNT,
        includeBots: Boolean = false
    ): List<Triple<String, Int, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Triple(ps.nickname, ps.strengthKillingSpree.first, ps.strengthKillingSpree.second)
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopSecondaryFireKillers(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getSecondaryFireKills(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopSecondaryFireKillersPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(
                ps.nickname,
                ps.getSecondaryFireKills(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0)
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopSecondaryFireVictims(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getSecondaryFireDeaths(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopSecondaryFireVictimsPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(
                ps.nickname,
                ps.getSecondaryFireDeaths(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0)
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopShotgunSmashers(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getShotgunSmasherCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopShotgunSmashersPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(
                ps.nickname,
                ps.getShotgunSmasherCount(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0)
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopShotgunSmasherDeaths(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getShotgunSmasherDeathCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopShotgunSmasherDeathssPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(
                ps.nickname,
                ps.getShotgunSmasherDeathCount(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0)
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopSplashKills(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getSplashKills(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopSplashKillsPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getSplashKills(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopSplashDeaths(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getSplashDeaths(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopSplashDeathsPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getSplashDeaths(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopCtfActions(
        count: Int = DEFAULT_COUNT,
        ctfAction: XonoticCtfActionType = XonoticCtfActionType.CAPTURE,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getCtfActionCount(ctfAction))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopCtfActionsPerPlaytime(
        count: Int = DEFAULT_COUNT,
        ctfAction: XonoticCtfActionType = XonoticCtfActionType.CAPTURE,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            val playtime = ps.getPlaytimeInSeconds(type, true).toDouble()
            Pair(
                ps.nickname,
                if(playtime.toInt() != 0) ps.getCtfActionCount(ctfAction).toDouble() / (playtime / 60.0) else 0.0
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopFlagCarrierKills(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getFlagCarrierKillCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopFlagCarrierKillsPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { ((includeBots && it.isBot) || !it.isBot) && it.flagCarrierKillCount != 0 }
            .map { ps ->
                Pair(
                    ps.nickname,
                    ps.getFlagCarrierKillCount(type).toDouble() / (ps.getPlaytimeInSeconds(type, true)
                        .toDouble() / 60.0)
                )
            }.sortedByDescending { it.second }.take(count)
    }

    fun getTopTypeKillers(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getTyperKillCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopTypeKillersPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getTyperKillCount(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopTypeDeaths(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getTypingDeathsCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopTypeDeathsPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(
                ps.nickname,
                ps.getTypingDeathsCount(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0)
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopStrengthKills(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getStrengthBuffKillCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopStrengthKillsPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(
                ps.nickname,
                ps.getStrengthBuffKillCount(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0)
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopStrengthDeaths(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Int>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getStrengthBuffDeathCount(type))
        }.sortedByDescending { it.second }.take(count)
    }

    fun getTopStrengthDeathsPerPlaytime(
        count: Int = DEFAULT_COUNT,
        type: EntityTypeFilter = EntityTypeFilter.ALL,
        includeBots: Boolean = false
    ): List<Pair<String, Double>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(
                ps.nickname,
                ps.getStrengthBuffDeathCount(type).toDouble() / (ps.getPlaytimeInSeconds(type).toDouble() / 60.0)
            )
        }.sortedByDescending { it.second }.take(count)
    }

    fun getPlayerVsPlayerKills(count: Int = DEFAULT_COUNT, includeBots: Boolean = false): List<Pair<String, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getOurVictims())
        }.sortedBy { it.first.toLowerCase() }.take(count)
    }

    fun getPlayerVsPlayerDeaths(count: Int = DEFAULT_COUNT, includeBots: Boolean = false): List<Pair<String, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getOurTopKillers())
        }.sortedBy { it.first.toLowerCase() }.take(count)
    }
    
    fun getPlayerWeaponKills(count: Int = DEFAULT_COUNT, includeBots: Boolean = false): List<Pair<String, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getWeaponKills())
        }.sortedBy { it.first.toLowerCase() }.take(count)
    }

    fun getPlayerWeaponKillsPerPlaytime(count: Int = DEFAULT_COUNT, includeBots: Boolean = false): List<Pair<String, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getWeaponKillsPerPlaytime())
        }.sortedBy { it.first }.take(count)
    }

    fun getPlayerWeaponDeaths(count: Int = DEFAULT_COUNT, includeBots: Boolean = false): List<Pair<String, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getWeaponDeaths())
        }.sortedBy { it.first.toLowerCase() }.take(count)
    }

    fun getPlayerWeaponDeathsPerPlaytime(count: Int = DEFAULT_COUNT, includeBots: Boolean = false): List<Pair<String, String>> {
        return players.values.filter { (includeBots && it.isBot) || !it.isBot }.map { ps ->
            Pair(ps.nickname, ps.getWeaponDeathsPerPlaytime())
        }.sortedBy { it.first }.take(count)
    }
}