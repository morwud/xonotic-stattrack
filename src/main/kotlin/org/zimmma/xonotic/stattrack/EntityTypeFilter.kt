package org.zimmma.xonotic.stattrack

/**
 * Represents type filter for excluding or including players and bots for results.
 *
 * @author Marek Zimmermann
 */
enum class EntityTypeFilter {
    ALL, PLAYERS, BOTS
}