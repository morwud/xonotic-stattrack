package org.zimmma.xonotic.stattrack.process.kill

import org.zimmma.xonotic.stattrack.process.XonoticDeathType
import org.zimmma.xonotic.stattrack.process.XonoticItemString
import org.zimmma.xonotic.stattrack.process.XonoticWeapon
import org.zimmma.xonotic.stattrack.process.getItemString
import java.time.LocalDateTime

/**
 * Class represents one team kill event.
 *
 * @author Marek Zimmermann
 */
data class XonoticTeamKill(
        val killerId: Int = 0,
        val victimId: Int = 0,
        val deathType: List<XonoticDeathType> = listOf(XonoticDeathType.ACCIDENT_TRAP),
        val weapon: XonoticWeapon = XonoticWeapon.UNKNOWN,
        val killerItemString: XonoticItemString = XonoticItemString(),
        val victimItemString: XonoticItemString = XonoticItemString(),
        val time: LocalDateTime? = null
) {
    constructor(killerId: Int, victimId: Int, fragValue: Int, killerItemString: String, victimItemString: String) : this(
            killerId = killerId,
            victimId = victimId,
            deathType = XonoticDeathType.getDeathTypes(fragValue),
            weapon = XonoticWeapon.getWeaponType(fragValue - XonoticDeathType.getDeathTypes(fragValue).sumBy { it.flagValue }),
            killerItemString = getItemString(killerItemString),
            victimItemString = getItemString(victimItemString)
    )
}