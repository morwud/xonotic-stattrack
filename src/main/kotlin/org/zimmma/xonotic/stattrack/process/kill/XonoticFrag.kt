package org.zimmma.xonotic.stattrack.process.kill

import org.zimmma.xonotic.stattrack.process.XonoticDeathType
import org.zimmma.xonotic.stattrack.process.XonoticItemString
import org.zimmma.xonotic.stattrack.process.XonoticWeapon
import org.zimmma.xonotic.stattrack.process.getItemString
import java.time.LocalDateTime

/**
 * Class representing one frag event on a map.
 *
 * @author Marek Zimmermann
 */
data class XonoticFrag(
        val killerId: Int = 0,
        val victimId: Int = 0,
        val deathType: List<XonoticDeathType> = listOf(XonoticDeathType.PRIMARY),
        val weapon: XonoticWeapon = XonoticWeapon.UNKNOWN,
        val killerItemString: XonoticItemString = XonoticItemString(),
        val victimItemString: XonoticItemString = XonoticItemString(),
        val time: LocalDateTime? = null
) {
    constructor(killerId: Int, victimId: Int, fragValue: Int, killerItemString: String, victimItemString: String) : this(
            killerId = killerId,
            victimId = victimId,
            deathType = XonoticDeathType.getDeathTypes(fragValue),
            weapon = XonoticWeapon.getWeaponType(fragValue - XonoticDeathType.getDeathTypes(fragValue).sumBy { it.flagValue }),
            killerItemString = getItemString(killerItemString),
            victimItemString = getItemString(victimItemString)
    )

}