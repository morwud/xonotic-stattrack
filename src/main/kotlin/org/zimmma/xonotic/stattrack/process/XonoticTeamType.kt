package org.zimmma.xonotic.stattrack.process

/**
 * Enum represents one team type.
 *
 * @author Marek Zimmermann
 */
enum class XonoticTeamType(val value: Int) {
    UNKNOWN(-1), NO_TEAM(1), RED_TEAM(5), PINK_TEAM(10), YELLOW_TEAM(13), BLUE_TEAM(14);
    companion object {
        fun getTeamType(value: Int): XonoticTeamType {
            for (teamType in values()) {
                if(value == teamType.value)
                    return teamType
            }
            
            throw IllegalArgumentException("Unknown team value '$value'")
        }
    }
}