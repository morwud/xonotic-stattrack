package org.zimmma.xonotic.stattrack

import kotlin.math.pow
import kotlin.math.round

/**
 * General util functions.
 *
 * @author Marek Zimmermann
 */

/**
 * Extension function to round a Double to [decimals] decimal places.
 */
fun Double.round(decimals: Int): Double {
    val multiplier = 10.0.pow(decimals)
    return round(this * multiplier) / multiplier
}