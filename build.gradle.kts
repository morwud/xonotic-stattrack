plugins {
    kotlin("jvm") version "1.4.20"
}

group = "org.zimmma.xonotic"
version = "0.1"

repositories {
    mavenCentral()
}

dependencies {
    /* Local deps in 'libs' folder */
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    
    implementation(kotlin("stdlib-jdk8"))
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}