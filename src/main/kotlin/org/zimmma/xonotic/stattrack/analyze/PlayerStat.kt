package org.zimmma.xonotic.stattrack.analyze

import org.zimmma.xonotic.stattrack.AccidentTypeFilter
import org.zimmma.xonotic.stattrack.EntityTypeFilter
import org.zimmma.xonotic.stattrack.process.XonoticDeathType
import org.zimmma.xonotic.stattrack.process.XonoticWeapon
import org.zimmma.xonotic.stattrack.process.ctf.XonoticCtfAction
import org.zimmma.xonotic.stattrack.process.ctf.XonoticCtfActionType
import org.zimmma.xonotic.stattrack.process.kill.XonoticAccident
import org.zimmma.xonotic.stattrack.process.vote.XonoticItemStringFlag
import org.zimmma.xonotic.stattrack.round

/**
 * Contains stats for one player.
 *
 * @author Marek Zimmermann
 */
data class PlayerStat(
    val nickname: String,
    val isBot: Boolean = nickname.startsWith("[BOT]"),

    val gameModePlayed: MutableMap<String, Int> = mutableMapOf(),
    val mapNamePlayed: MutableMap<String, Int> = mutableMapOf(),

    var dmgGiven: Double = 0.0,
    var dmgGivenBotless: Double = 0.0,
    var dmgTaken: Double = 0.0,
    var dmgTakenBotless: Double = 0.0,

    val playersKilled: MutableMap<String, Int> = mutableMapOf(),
    val killedBy: MutableMap<String, Int> = mutableMapOf(),

    var headshotsGiven: Int = 0,
    var headshotsGivenBotless: Int = 0,
    var headshotsRecieved: Int = 0,
    var headshotsRecievedBotless: Int = 0,

    var accidentCount: Int = 0,
    var accidentGeneral: Int = 0,
    var accidentWithStrengthCount: Int = 0,
    var accidentWithFlagCount: Int = 0,
    var acciedntLavaCount: Int = 0,
    var accidentFallCount: Int = 0,

    var totalPlaytimeSeconds: Int = 0,
    var totalBotlessPlaytimeSeconds: Int = 0,
    var totalCtfPlaytimeSeconds: Int = 0,
    var totalCtfBotlessPlaytimeSeconds: Int = 0,

    var firstBloodCounts: MutableMap<String, Int> = mutableMapOf(),
    var firstBloodVictimCounts: MutableMap<String, Int> = mutableMapOf(),

    var secondaryFireKillCount: Int = 0,
    var secondaryFireKillCountBotless: Int = 0,
    var secondaryFireDeathCount: Int = 0,
    var secondaryFireDeathCountBotless: Int = 0,

    var shotgunSmashCount: Int = 0,
    var shotgunSmashCountBotless: Int = 0,
    var shotgunSmashDeathCount: Int = 0,
    var shotgunSmashDeathCountBotless: Int = 0,

    var splashDeath: Int = 0,
    var splashDeathBotless: Int = 0,
    var splashKill: Int = 0,
    var splashKillBotless: Int = 0,

    var flagCarrierKillCount: Int = 0,
    var flagCarrierKillCountBotless: Int = 0,
    var typerKillCount: Int = 0,
    var typerKillCountBotless: Int = 0,

    var typingDeathsCount: Int = 0,
    var typingDeathsCountBotless: Int = 0,

    var strengthBuffKills: Int = 0,
    var strengthBuffKillsBotless: Int = 0,
    var strengthBuffDeaths: Int = 0,
    var strengthBuffDeathsBotless: Int = 0,

    var ctfCapturedCount: Int = 0,
    var ctfDroppedCount: Int = 0,
    var ctfPickupCount: Int = 0,
    var ctfReturnCount: Int = 0,
    var ctfStealCount: Int = 0,

    var killingSpree: Pair<Int, String> = Pair(0, ""),
    var strengthKillingSpree: Pair<Int, String> = Pair(0, ""),

    var weaponKillsBotless: MutableMap<XonoticWeapon, Int> = mutableMapOf(),
    var weaponDeathsBotless: MutableMap<XonoticWeapon, Int> = mutableMapOf()
) {
    fun addCtfAction(ctfAction: XonoticCtfAction) {
        when(ctfAction.actionType) {
            XonoticCtfActionType.CAPTURE -> ctfCapturedCount++
            XonoticCtfActionType.DROPPED -> ctfDroppedCount++
            XonoticCtfActionType.PICKUP -> ctfPickupCount++
            XonoticCtfActionType.RETURN -> ctfReturnCount++
            XonoticCtfActionType.STEAL -> ctfStealCount++
            else -> {}
        }
    }

    fun addFirstblood(victim: PlayerStat) {
        firstBloodCounts[victim.nickname] = firstBloodCounts[victim.nickname]?.plus(1) ?: 1
    }

    fun addFirstbloodVictim(killer: PlayerStat) {
        firstBloodVictimCounts[killer.nickname] = firstBloodVictimCounts[killer.nickname]?.plus(1) ?: 1
    }

    fun addPlaytime(seconds: Int, withBots: Boolean, ctfMode: Boolean = false) {
        if (!withBots)
            totalBotlessPlaytimeSeconds += seconds

        totalPlaytimeSeconds += seconds

        if(ctfMode) {
            totalCtfPlaytimeSeconds += seconds

            if(!withBots)
                totalCtfBotlessPlaytimeSeconds += seconds
        }
    }

    fun addDmgGiven(dmg: Double, withBots: Boolean) {
        if (!withBots)
            dmgGivenBotless += dmg

        dmgGiven += dmg
    }

    fun addDmgTaken(dmg: Double, withBots: Boolean) {
        if (!withBots)
            dmgTakenBotless += dmg

        dmgTaken += dmg
    }

    fun addPlayerKilled(victim: PlayerStat, deathType: List<XonoticDeathType>, weapon: XonoticWeapon, victimFlags: List<XonoticItemStringFlag>) {
        playersKilled[victim.nickname] = playersKilled[victim.nickname]?.plus(1) ?: 1

        if(victimFlags.contains(XonoticItemStringFlag.FLAG_CARRIER)) {
            flagCarrierKillCount++
            if (!victim.isBot) {
                flagCarrierKillCountBotless++
            }
        }
        if(victimFlags.contains(XonoticItemStringFlag.TYPING)){
            typerKillCount++
            if(!victim.isBot)
                typerKillCountBotless++
        }
        if (victimFlags.contains(XonoticItemStringFlag.STRENGTH)) {
            strengthBuffKills++
            if(!victim.isBot) {
                strengthBuffKillsBotless++
            }
        }

        for (death in deathType) {
            when (death) {
                XonoticDeathType.HEADSHOT -> {
                    headshotsGiven++
                    if (!victim.isBot)
                        headshotsGivenBotless++
                }
                XonoticDeathType.PRIMARY -> {
                    //we don't care - that is basically the normal death by weapon
                }
                XonoticDeathType.SECONDARY_FIRE -> {
                    secondaryFireKillCount++
                    if(!victim.isBot)
                        secondaryFireKillCountBotless++

                    if(weapon == XonoticWeapon.SHOTGUN) {
                        shotgunSmashCount++
                        if(!victim.isBot)
                            shotgunSmashCountBotless++
                    }
                }
                XonoticDeathType.SPLASH_DAMAGE -> {
                    splashKill++
                    if(!victim.isBot)
                        splashKillBotless++
                }
                else -> println("Unprocessed death type: $death of victim ${victim.nickname}")
            }
        }

        if(!victim.isBot && weapon != XonoticWeapon.UNKNOWN) {
            weaponKillsBotless[weapon] = weaponKillsBotless[weapon]?.plus(1) ?: 1
        }
    }

    fun addDeath(killer: PlayerStat, deathType: List<XonoticDeathType>, weapon: XonoticWeapon, victimFlags: List<XonoticItemStringFlag>) {
        killedBy[killer.nickname] = killedBy[killer.nickname]?.plus(1) ?: 1

        if(victimFlags.contains(XonoticItemStringFlag.TYPING)) {
            typingDeathsCount++
            if(!killer.isBot) {
                typingDeathsCountBotless++
            }
        }
        if (victimFlags.contains(XonoticItemStringFlag.STRENGTH)) {
            strengthBuffDeaths++
            if(!killer.isBot) {
                strengthBuffDeathsBotless++
            }
        }
        
        for (death in deathType) {
            when (death) {
                XonoticDeathType.HEADSHOT -> {
                    headshotsRecieved++
                    if (!killer.isBot)
                        headshotsGivenBotless++
                }
                XonoticDeathType.PRIMARY -> {
                    //we don't care - that is basically the normal death by weapon
                }
                XonoticDeathType.SECONDARY_FIRE -> {
                    secondaryFireDeathCount++
                    if(!killer.isBot)
                        secondaryFireDeathCountBotless++

                    if(weapon == XonoticWeapon.SHOTGUN) {
                        shotgunSmashDeathCount++
                        if(!killer.isBot) {
                            shotgunSmashDeathCountBotless++
                        }
                    }
                }
                XonoticDeathType.SPLASH_DAMAGE -> {
                    splashDeath++
                    if(!killer.isBot)
                        splashDeathBotless++
                }
                else -> println("Unprocessed death type: $death of killer ${killer.nickname}")
            }
        }

        if(!killer.isBot && weapon != XonoticWeapon.UNKNOWN) {
            weaponDeathsBotless[weapon] = weaponDeathsBotless[weapon]?.plus(1) ?: 1
        }
    }

    fun addAccident(accident: XonoticAccident) {
        accidentCount++

        for (flag in accident.victimItemString.flags) {
            when (flag) {
                XonoticItemStringFlag.STRENGTH -> accidentWithStrengthCount++
                XonoticItemStringFlag.FLAG_CARRIER -> accidentWithFlagCount++
                else -> {
                    println("Unprocessed flag: $flag in accident $accident")
                }
            }
        }

        when (accident.deathType) {
            XonoticDeathType.LAVA -> acciedntLavaCount++
            XonoticDeathType.FALL -> accidentFallCount++
            XonoticDeathType.ACCIDENT_TRAP -> accidentGeneral++
            else -> {
                println("Unprocessed death type ${accident.deathType} in $accident")
            }
        }
    }

    fun getKillCount(type: EntityTypeFilter = EntityTypeFilter.ALL): Int {
        return playersKilled.filter {
            when (type) {
                EntityTypeFilter.ALL -> {
                    true
                }
                EntityTypeFilter.PLAYERS -> {
                    !it.key.contains("[BOT]")
                }
                EntityTypeFilter.BOTS -> {
                    it.key.contains("[BOT]")
                }
            }
        }.map { it.value }.sum()
    }
    
    fun getOurVictims(): String {
        val filteredVictims = playersKilled.filter { !it.key.contains("[BOT]") }

            return filteredVictims.toList().sortedByDescending { it.second }.joinToString { "[${it.first},${it.second}]" }
    }

    fun getDeathCount(type: EntityTypeFilter = EntityTypeFilter.ALL): Int {
        return killedBy.filter {
            when (type) {
                EntityTypeFilter.ALL -> {
                    true
                }
                EntityTypeFilter.PLAYERS -> {
                    !it.key.contains("[BOT]")
                }
                EntityTypeFilter.BOTS -> {
                    it.key.contains("[BOT]")
                }
            }
        }.map { it.value }.sum()
    }
    
    fun getOurTopKillers(): String {
        val filteredKillers = killedBy.filter { !it.key.contains("[BOT]") }

            return filteredKillers.toList().sortedByDescending { it.second }.joinToString { "[${it.first},${it.second}]" }
    }

    fun getAccidentsCount(type: AccidentTypeFilter = AccidentTypeFilter.ALL): Int {
        return when (type) {
            AccidentTypeFilter.ALL -> accidentCount
            AccidentTypeFilter.FLAG_ONLY -> accidentWithFlagCount
            AccidentTypeFilter.STRENGTH_ONLY -> accidentWithStrengthCount
            AccidentTypeFilter.LAVA_ONLY -> acciedntLavaCount
            AccidentTypeFilter.FALL_ONLY -> accidentFallCount
            AccidentTypeFilter.GENERAL_ONLY -> accidentGeneral // these are basically all the accidents
        }
    }

    fun getPlaytimeInSeconds(type: EntityTypeFilter = EntityTypeFilter.ALL, ctfMode: Boolean = false): Int {
        if(ctfMode) {
            return when (type) {
                EntityTypeFilter.ALL -> totalCtfPlaytimeSeconds
                EntityTypeFilter.PLAYERS -> totalCtfBotlessPlaytimeSeconds
                EntityTypeFilter.BOTS -> totalCtfPlaytimeSeconds - totalCtfBotlessPlaytimeSeconds
            }
        } else {
            return when (type) {
                EntityTypeFilter.ALL -> totalPlaytimeSeconds
                EntityTypeFilter.PLAYERS -> totalBotlessPlaytimeSeconds
                EntityTypeFilter.BOTS -> totalPlaytimeSeconds - totalBotlessPlaytimeSeconds
            }
        }
    }

    fun getFirstBloodCounts(): Int {
        return firstBloodCounts.values.sum()
    }

    fun getFirstBloodVictimCounts(): Int {
        return firstBloodVictimCounts.values.sum()
    }

    fun addGamePlayed(gameMode: String, name: String) {
        gameModePlayed[gameMode] = gameModePlayed[gameMode]?.plus(1) ?: 1
        mapNamePlayed[name] = mapNamePlayed[name]?.plus(1) ?: 1
    }

    fun getTotalGamesPlayed(): Int {
        return gameModePlayed.values.sum()
    }

    fun getHeadshotsGiven(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> headshotsGivenBotless
            EntityTypeFilter.BOTS -> headshotsGiven - headshotsGivenBotless
            EntityTypeFilter.ALL -> headshotsGiven
        }
    }

    fun getHeadshotsTaken(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> headshotsRecievedBotless
            EntityTypeFilter.BOTS -> headshotsRecieved - headshotsRecievedBotless
            EntityTypeFilter.ALL -> headshotsRecieved
        }
    }
    
    fun setKillingSpree(mapName: String, spreeCount: Int) {
        if(spreeCount > killingSpree.first) {
            killingSpree = Pair(spreeCount, mapName)
        } else if(spreeCount == killingSpree.first && !killingSpree.second.contains(mapName)) {
            killingSpree = Pair(spreeCount, "${killingSpree.second}, $mapName")
        }
    }
    
    fun setStrengthKillingSpree(mapName: String, spreeCount: Int) {
        if(spreeCount > strengthKillingSpree.first) {
            strengthKillingSpree = Pair(spreeCount, mapName)
        } else if(spreeCount == strengthKillingSpree.first && !strengthKillingSpree.second.contains(mapName)) {
            strengthKillingSpree = Pair(spreeCount, "${strengthKillingSpree.second}, $mapName")
        }
    }
    
    fun getSecondaryFireKills(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> secondaryFireKillCountBotless
            EntityTypeFilter.BOTS -> secondaryFireKillCount - secondaryFireKillCountBotless
            EntityTypeFilter.ALL -> secondaryFireKillCount
        }
    }
    
    fun getSecondaryFireDeaths(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> secondaryFireDeathCountBotless
            EntityTypeFilter.BOTS -> secondaryFireDeathCount - secondaryFireDeathCountBotless
            EntityTypeFilter.ALL -> secondaryFireDeathCount
        }
    }
    
    fun getShotgunSmasherCount(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> shotgunSmashCountBotless
            EntityTypeFilter.BOTS -> shotgunSmashCount - shotgunSmashCountBotless
            EntityTypeFilter.ALL -> shotgunSmashCount
        }
    }

    fun getShotgunSmasherDeathCount(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> shotgunSmashDeathCountBotless
            EntityTypeFilter.BOTS -> shotgunSmashDeathCount - shotgunSmashDeathCountBotless
            EntityTypeFilter.ALL -> shotgunSmashDeathCount
        }
    }
    
    fun getFlagCarrierKillCount(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> flagCarrierKillCountBotless
            EntityTypeFilter.BOTS -> flagCarrierKillCount - flagCarrierKillCountBotless
            EntityTypeFilter.ALL -> flagCarrierKillCount
        }
    }
    
    fun getTyperKillCount(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> typerKillCountBotless
            EntityTypeFilter.BOTS -> typerKillCount - typerKillCountBotless
            EntityTypeFilter.ALL -> typerKillCount
        }
    }
    
    fun getTypingDeathsCount(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> typingDeathsCountBotless
            EntityTypeFilter.BOTS -> typingDeathsCount - typingDeathsCountBotless
            EntityTypeFilter.ALL -> typingDeathsCount
        }
    }
    
    fun getStrengthBuffKillCount(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> strengthBuffKillsBotless
            EntityTypeFilter.BOTS -> strengthBuffKills - strengthBuffKillsBotless
            EntityTypeFilter.ALL -> strengthBuffKills
        }
    }
    
    fun getStrengthBuffDeathCount(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> strengthBuffDeathsBotless
            EntityTypeFilter.BOTS -> strengthBuffDeaths - strengthBuffDeathsBotless
            EntityTypeFilter.ALL -> strengthBuffDeaths
        }
    }
    
    fun getCtfActionCount(action: XonoticCtfActionType): Int {
        return when (action) {
            XonoticCtfActionType.CAPTURE -> ctfCapturedCount
            XonoticCtfActionType.DROPPED -> ctfDroppedCount
            XonoticCtfActionType.PICKUP -> ctfPickupCount
            XonoticCtfActionType.RETURN -> ctfReturnCount
            XonoticCtfActionType.STEAL -> ctfStealCount
            else -> 0
        }
    }
    
    fun getSplashKills(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> splashDeathBotless
            EntityTypeFilter.BOTS -> splashDeath - splashDeathBotless
            EntityTypeFilter.ALL -> splashDeath
        }
    }
    
    fun getSplashDeaths(type: EntityTypeFilter): Int {
        return when (type) {
            EntityTypeFilter.PLAYERS -> splashKillBotless
            EntityTypeFilter.BOTS -> splashKill - splashKillBotless
            EntityTypeFilter.ALL -> splashKill
        }
    }
    
    fun getWeaponKills(): String {
        return weaponKillsBotless.toList().sortedByDescending { it.second }.joinToString { "${it.first} (${it.second})" }
    }

    fun getWeaponKillsPerPlaytime(): String {
        return weaponKillsBotless.toList().map { Pair(it.first, (it.second.toDouble() / (getPlaytimeInSeconds(EntityTypeFilter.PLAYERS) / 60.0))) }.sortedByDescending { it.second }.map { Pair(it.first,it.second.round(2)) }.joinToString { "${it.first} (${it.second})" }
    }

    fun getWeaponDeaths(): String {
        return weaponDeathsBotless.toList().sortedByDescending { it.second }.joinToString { "${it.first} (${it.second})" }
    }

    fun getWeaponDeathsPerPlaytime(): String {
        return weaponDeathsBotless.toList().map { Pair(it.first, it.second.toDouble() / (getPlaytimeInSeconds(EntityTypeFilter.PLAYERS) / 60.0)) }.sortedByDescending { it.second }.map { Pair(it.first,it.second.round(2)) }.joinToString { "${it.first} (${it.second})" }
    }
}