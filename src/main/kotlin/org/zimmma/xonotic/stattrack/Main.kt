package org.zimmma.xonotic.stattrack

import ml.options.Options
import org.zimmma.xonotic.stattrack.analyze.MapStat
import org.zimmma.xonotic.stattrack.analyze.PlayerStat
import org.zimmma.xonotic.stattrack.analyze.StatResults
import org.zimmma.xonotic.stattrack.process.XonoticMap
import org.zimmma.xonotic.stattrack.process.ctf.XonoticCtfActionType
import org.zimmma.xonotic.stattrack.process.kill.XonoticAccident
import org.zimmma.xonotic.stattrack.process.kill.XonoticFrag
import org.zimmma.xonotic.stattrack.process.processLogs
import org.zimmma.xonotic.stattrack.process.vote.XonoticItemStringFlag
import java.io.File
import kotlin.system.exitProcess

const val DEFAULT_DATA_FOLDER_PATH = "data/"

const val DEFAULT_EVENTS_FILE_PREFIX = "ev_"

var withPlayers = true
var withBots = false

/**
 * Program that process some stats from Xonotic logs and then displays them into a console.
 *
 * @author Marek Zimmermann
 */
fun main(args: Array<String>) {
    // https://www.javaworld.com/article/2074849/processing-command-line-arguments-in-java--case-closed.html
    val options = Options(args)

    //TODO add params
    val dataFolderPath = DEFAULT_DATA_FOLDER_PATH
    val eventsFileNamePrefix = DEFAULT_EVENTS_FILE_PREFIX

    val dataFolder = File(dataFolderPath)
    if (!dataFolder.isDirectory) {
        println("Given path is not a directory!")
        exitProcess(1)
    }

    // load all log files
    val processedLogs = processLogs(dataFolder, eventsFileNamePrefix)
    println("${processedLogs.size} maps are usable for analysis")

    // do some analysis
    val statResults = analyze(processedLogs)

    // print the raw results
    browseResults(statResults)
}

private fun browseResults(statResults: StatResults) {
    var input: String? = null
    var inputSplit: List<String>? = null
    println("Processing complete - you can now browse results")

    while ({ print("> ");input = readLine(); input }() != null || input != "exit") {
        when ({ inputSplit = input?.trim()?.split(" ");inputSplit?.get(0) ?: "" }()) {
            "exit", "quit", "q" -> return
            "help", "h" -> {
                printHelp()
            }
            "abs" -> {
                processAbsoluteStatRequests(inputSplit, statResults)
            }
            "spm" -> {
                processPerMinuteRequests(inputSplit, statResults)
            }
            "tbots" -> {
                withBots = !withBots
                println("With bots now set to: $withBots")
            }
            "tplayers" -> {
                withPlayers = !withPlayers
                println("With players now set to: $withPlayers")
            }
            else -> {
                println("Unknown command '$input'")
            }
        }
    }
}

private fun processPerMinuteRequests(inputSplit: List<String>?, statResults: StatResults) {
    if ((inputSplit?.size ?: 0) <= 1) {
        printHelp()
    } else {
        when (inputSplit?.get(1)?.trim()) {
            "dmgTaken" -> {
                printStat("TOP TOTAL DAMAGE TAKEN PER MINUTE") {
                    for ((index, pair) in statResults.getTopDmgTakenPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "dmgGiven" -> {
                printStat("TOP TOTAL DAMAGE GIVEN PER MINUTE") {
                    for ((index, pair) in statResults.getTopDmgGivenPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "killers" -> {
                printStat("TOP PLAYER KILLERS PER MINUTE") {
                    for ((index, pair) in statResults.getTopKillersPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "victims" -> {
                printStat("TOP PLAYER VICTIMS PER MINUTE") {
                    for ((index, pair) in statResults.getTopDeadPlayersPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "firstblood" -> {
                printStat("TOP FIRSTBLOOD PLAYERS PER MINUTE") {
                    for ((index, pair) in statResults.getTopFirstbloodPlayersPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "firstbloodVic" -> {
                printStat("TOP FIRSTBLOOD PLAYER VICTIMS PER MINUTE") {
                    for ((index, pair) in statResults.getTopFirstbloodPlayerVictimssPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "killsPerMap" -> {
                printStat("TOP KILLERS PER MAPS PLAYED") {
                    for ((index, pair) in statResults.getKillsPerMapsPlayed(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "headshotGive" -> {
                printStat("TOP HEADSHOT GIVERS PER MINUTE") {
                    for ((index, pair) in statResults.getTopHeadshotsGiversPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "headshotTake" -> {
                printStat("TOP HEADSHOT TAKERS PER MINUTE") {
                    for ((index, pair) in statResults.getTopHeadshotsTakersPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "secondaryFireKillers" -> {
                printStat("TOP SECONDARY FIRE KILLERS PER MINUTE") {
                    for ((index, pair) in statResults.getTopSecondaryFireKillersPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "secondaryFireVictims" -> {
                printStat("TOP SECONDARY FIRE VICTIMS PER MINUTE") {
                    for ((index, pair) in statResults.getTopSecondaryFireVictimsPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "shotgunSmashers" -> {
                printStat("TOP SHOTGUN SMASHERS PER MINUTE") {
                    for ((index, pair) in statResults.getTopShotgunSmashersPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "shotgunSmasherDeaths" -> {
                printStat("TOP SHOTGUN SMASHER DEATHS PER MINUTE") {
                    for ((index, pair) in statResults.getTopShotgunSmasherDeathssPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "flagCarrierKills" -> {
                printStat("TOP FLAG CARRIER KILLS PER MINUTE") {
                    for ((index, pair) in statResults.getTopFlagCarrierKillsPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "typerKillers" -> {
                printStat("TOP TYPER KILLERS PER MINUTE") {
                    for ((index, pair) in statResults.getTopTypeKillersPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "typeDeaths" -> {
                printStat("TOP TYPING DEATHS PER MINUTE") {
                    for ((index, pair) in statResults.getTopTypeDeathsPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "winnersPM" -> {
                printStat("TOP WINNERS PER MAP PER PLAYER'S PRESENCE") {
                    for ((index, pair) in statResults.getWinnersByMapPerPresence().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "splash" -> {
                printStat("TOP SPLASH KILLERS PER MINUTE") {
                    for ((index, pair) in statResults.getTopSplashKillsPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "splashD" -> {
                printStat("TOP SPLASH VICTIMS PER MINUTE") {
                    for ((index, pair) in statResults.getTopSplashDeathsPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "strength" -> {
                printStat("TOP KILLERS WITH STRENGTH BUFF PER MINUTE") {
                    for ((index, pair) in statResults.getTopStrengthKillsPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "strengthD" -> {
                printStat("TOP VICTIMS WITH STRENGTH BUFF PER MINUTE") {
                    for ((index, pair) in statResults.getTopStrengthDeathsPerPlaytime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "weapons" -> {
                printStat("TOP WEAPON KILLS PER MINUTE") {
                    for (pair in statResults.getPlayerWeaponKillsPerPlaytime()) {
                        println("${pair.first} - ${pair.second}")
                    }
                }
            }
            "weaponsD" -> {
                printStat("TOP WEAPON DEATHS PER MINUTE") {
                    for (pair in statResults.getPlayerWeaponDeathsPerPlaytime()) {
                        println("${pair.first} - ${pair.second}")
                    }
                }
            }
            "ctf" -> {
                if (inputSplit.size <= 2) {
                    // nope
                } else {
                    when (inputSplit[2]) {
                        "captured" -> {
                            printStat("TOP TOTAL FLAG CAPTURES PER MINUTE") {
                                for ((index, pair) in statResults.getTopCtfActionsPerPlaytime(ctfAction = XonoticCtfActionType.CAPTURE, type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                                }
                            }
                        }
                        "drops" -> {
                            printStat("TOP TOTAL FLAG DROPS PER MINUTE") {
                                for ((index, pair) in statResults.getTopCtfActionsPerPlaytime(ctfAction = XonoticCtfActionType.DROPPED, type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                                }
                            }
                        }
                        "pickups" -> {
                            printStat("TOP TOTAL FLAG PICKUPS PER MINUTE") {
                                for ((index, pair) in statResults.getTopCtfActionsPerPlaytime(ctfAction = XonoticCtfActionType.PICKUP, type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                                }
                            }
                        }
                        "returns" -> {
                            printStat("TOP TOTAL FLAG RETURNS PER MINUTE") {
                                for ((index, pair) in statResults.getTopCtfActionsPerPlaytime(ctfAction = XonoticCtfActionType.RETURN, type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                                }
                            }
                        }
                        "steals" -> {
                            printStat("TOP TOTAL FLAG STEALS PER MINUTE") {
                                for ((index, pair) in statResults.getTopCtfActionsPerPlaytime(ctfAction = XonoticCtfActionType.STEAL, type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                                }
                            }
                        }
                        else -> {
                            println("Unknown command '${inputSplit.joinToString(separator = " ")}'")
                        }
                    }
                }
            }
            "accidents" -> {
                if (inputSplit.size <= 2) {
                    printStat("TOP TOTAL ACCIDENTS PER MINUTE") {
                        for ((index, pair) in statResults.getTopAccidentsPerPlaytime(type = AccidentTypeFilter.ALL).withIndex()) {
                            println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                        }
                    }
                } else {
                    when (inputSplit[2]) {
                        "general" -> {
                            printStat("TOP TOTAL GENERAL ACCIDENTS PER MINUTE") {
                                for ((index, pair) in statResults.getTopAccidentsPerPlaytime(type = AccidentTypeFilter.GENERAL_ONLY).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                                }
                            }
                        }
                        "strength" -> {
                            printStat("TOP TOTAL STRENGTH ACCIDENTS PER MINUTE") {
                                for ((index, pair) in statResults.getTopAccidentsPerPlaytime(type = AccidentTypeFilter.STRENGTH_ONLY).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                                }
                            }
                        }
                        "flag" -> {
                            printStat("TOP TOTAL FLAG ACCIDENTS PER MINUTE") {
                                for ((index, pair) in statResults.getTopAccidentsPerPlaytime(type = AccidentTypeFilter.FLAG_ONLY).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                                }
                            }
                        }
                        else -> {
                            println("Unknown command '${inputSplit.joinToString(separator = " ")}'")
                        }
                    }
                }
            }
        }
    }
}

private fun processAbsoluteStatRequests(inputSplit: List<String>?, statResults: StatResults) {
    if ((inputSplit?.size ?: 0) <= 1) {
        printHelp()
    } else {
        when (inputSplit?.get(1)?.trim()) {
            "dmgTaken" -> {
                printStat("TOP TOTAL DAMAGE TAKEN") {
                    for ((index, pair) in statResults.getTopDmgTaken(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "dmgGiven" -> {
                printStat("TOP TOTAL DAMAGE GIVEN") {
                    for ((index, pair) in statResults.getTopDmgGiven(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "killers" -> {
                printStat("TOP PLAYER KILLERS") {
                    for ((index, pair) in statResults.getTopKillers(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "victims" -> {
                printStat("TOP PLAYER VICTIMS") {
                    for ((index, pair) in statResults.getTopDeadPlayers(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "firstblood" -> {
                printStat("TOP FIRSTBLOOD PLAYERS") {
                    for ((index, pair) in statResults.getTopFirstbloodPlayers().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "firstbloodVic" -> {
                printStat("TOP FIRSTBLOOD PLAYER VICTIMS") {
                    for ((index, pair) in statResults.getTopFirstbloodPlayerVictims().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "gamemodePM" -> {
                printStat("TOP PLAYED GAMEMODES PER MAP") {
                    for ((index, pair) in statResults.getTopFavouriteGameModePerMapName().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "gamemodePP" -> {
                printStat("TOP PLAYED GAMEMODES PER PLAYER") {
                    for (pair in statResults.getTopFavouriteGameModePerPlayer()) {
                        println("${pair.first} - ${pair.second}")
                    }
                }
            }
            "killingSpree" -> {
                printStat("LARGEST KILLING SPREE") {
                    for (triple in statResults.getTopKillingSpree()) {
                        println("${triple.first} - ${triple.second} (${triple.third})")
                    }
                }
            }
            "strengthKillingSpree" -> {
                printStat("LARGEST KILLING SPREE WITH STRENGTH BUFF") {
                    for (triple in statResults.getTopStrengthKillingSpree()) {
                        println("${triple.first} - ${triple.second} (${triple.third})")
                    }
                }
            }
            "mapnamePM" -> {
                printStat("TOP PLAYED MAPS") {
                    for ((index, pair) in statResults.getTopFavouriteMapNamePerMapName().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "mapnamePMInverted" -> {
                printStat("TOP PLAYED MAPS") {
                    for ((index, pair) in statResults.getTopFavouriteMapNamePerMapName(inverted = true).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "mapnamePP" -> {
                printStat("TOP PLAYED MAPS PER PLAYER") {
                    for (pair in statResults.getTopFavouriteMapNamePerPlayer()) {
                        println("${pair.first} - ${pair.second}")
                    }
                }
            }
            "gamesplayed" -> {
                printStat("TOP MAPS PLAYED PLAYERS") {
                    for (pair in statResults.getTopMapsPlayedPlayers()) {
                        println("${pair.first} - ${pair.second}")
                    }
                }
            }
            "playTime" -> {
                printStat("TOP PLAYTIME PER PLAYER") {
                    for ((index, pair) in statResults.getTotalPlayTime(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "headshotGive" -> {
                printStat("TOP HEADSHOT GIVERS") {
                    for ((index, pair) in statResults.getTopHeadshotsGivers(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "headshotTake" -> {
                printStat("TOP HEADSHOT TAKERS") {
                    for ((index, pair) in statResults.getTopHeadshotsTakers(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "secondaryFireKillers" -> {
                printStat("TOP SECONDARY FIRE KILLERS") {
                    for ((index, pair) in statResults.getTopSecondaryFireKillers(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "secondaryFireVictims" -> {
                printStat("TOP SECONDARY FIRE VICTIMS") {
                    for ((index, pair) in statResults.getTopSecondaryFireVictims(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "shotgunSmashers" -> {
                printStat("TOP SHOTGUN SMASHERS") {
                    for ((index, pair) in statResults.getTopShotgunSmashers(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "shotgunSmasherDeaths" -> {
                printStat("TOP SHOTGUN SMASHER DEATHS") {
                    for ((index, pair) in statResults.getTopShotgunSmasherDeaths(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "flagCarrierKills" -> {
                printStat("TOP FLAG CARRIER KILLERS") {
                    for ((index, pair) in statResults.getTopFlagCarrierKills(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "typerKillers" -> {
                printStat("TOP TYPER KILLERS") {
                    for ((index, pair) in statResults.getTopTypeKillers(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "typeDeaths" -> {
                printStat("TOP TYPING DEATHS") {
                    for ((index, pair) in statResults.getTopTypeDeaths(type = getEntityFilter(withPlayers, withBots)).withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "mapKills" -> {
                printStat("TOP KILL COUNT PER MAP") {
                    for ((index, pair) in statResults.getMapWithTopKills().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "mapKillsPP" -> {
                printStat("TOP KILL COUNT PER MAP PER PLAYER COUNT") {
                    for ((index, pair) in statResults.getMapWithTopKillsPerPlayers().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "mapSuicides" -> {
                printStat("TOP SUICIDE COUNT PER MAP") {
                    for ((index, pair) in statResults.getMapWithTopSuicides().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "mapSuicidesPP" -> {
                printStat("TOP SUICIDE COUNT PER MAP PER PLAYER COUNT") {
                    for ((index, pair) in statResults.getMapWithTopSuicidesPerPlayers().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "mapAccidents" -> {
                printStat("TOP ACCIDENTS COUNT PER MAP") {
                    for ((index, pair) in statResults.getMapWithTopAccidents().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "mapAccidentsPP" -> {
                printStat("TOP ACCIDENTS COUNT PER MAP PER PLAYER COUNT") {
                    for ((index, pair) in statResults.getMapWithTopAccidentsPerPlayers().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second.round(2)}")
                    }
                }
            }
            "winnersPM" -> {
                printStat("TOP WINNERS PER MAP") {
                    for ((index, pair) in statResults.getWinnersByMapAbsolute().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "splash" -> {
                printStat("TOP KILLERS WITH SPLASH DAMAGE") {
                    for ((index, pair) in statResults.getTopSplashKills().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "splashD" -> {
                printStat("TOP VICTIMS WITH SPLASH DAMAGE") {
                    for ((index, pair) in statResults.getTopSplashDeaths().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "strength" -> {
                printStat("TOP KILLERS WITH STRENGTH BUFF") {
                    for ((index, pair) in statResults.getTopStrengthKills().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "strengthD" -> {
                printStat("TOP VICTIMS WITH STRENGTH BUFF") {
                    for ((index, pair) in statResults.getTopStrengthDeaths().withIndex()) {
                        println("${index + 1}. ${pair.first} - ${pair.second}")
                    }
                }
            }
            "pvpKills" -> {
                printStat("TOP PVP KILLS") {
                    for (pair in statResults.getPlayerVsPlayerKills()) {
                        println("${pair.first} - ${pair.second}")
                    }
                }
            }
            "pvpDeaths" -> {
                printStat("TOP PVP DEATHS") {
                    for (pair in statResults.getPlayerVsPlayerDeaths()) {
                        println("${pair.first} - ${pair.second}")
                    }
                }
            }
            "weapons" -> {
                printStat("TOP WEAPON KILLS") {
                    for (pair in statResults.getPlayerWeaponKills()) {
                        println("${pair.first} - ${pair.second}")
                    }
                }
            }
            "weaponsD" -> {
                printStat("TOP WEAPON DEATHS") {
                    for (pair in statResults.getPlayerWeaponDeaths()) {
                        println("${pair.first} - ${pair.second}")
                    }
                }
            }
            "ctf" -> {
                if (inputSplit.size <= 2) {
                    // nope
                } else {
                    when (inputSplit[2]) {
                        "captured" -> {
                            printStat("TOP TOTAL FLAG CAPTURES") {
                                for ((index, pair) in statResults.getTopCtfActions(ctfAction = XonoticCtfActionType.CAPTURE).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second}")
                                }
                            }
                        }
                        "drops" -> {
                            printStat("TOP TOTAL FLAG DROPS") {
                                for ((index, pair) in statResults.getTopCtfActions(ctfAction = XonoticCtfActionType.DROPPED).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second}")
                                }
                            }
                        }
                        "pickups" -> {
                            printStat("TOP TOTAL FLAG PICKUPS") {
                                for ((index, pair) in statResults.getTopCtfActions(ctfAction = XonoticCtfActionType.PICKUP).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second}")
                                }
                            }
                        }
                        "returns" -> {
                            printStat("TOP TOTAL FLAG RETURNS") {
                                for ((index, pair) in statResults.getTopCtfActions(ctfAction = XonoticCtfActionType.RETURN).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second}")
                                }
                            }
                        }
                        "steals" -> {
                            printStat("TOP TOTAL FLAG STEALS") {
                                for ((index, pair) in statResults.getTopCtfActions(ctfAction = XonoticCtfActionType.STEAL).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second}")
                                }
                            }
                        }
                        else -> {
                            println("Unknown CTF command '${inputSplit.joinToString(separator = " ")}'")
                        }
                    }
                }
            }
            "accidents" -> {
                if (inputSplit.size <= 2) {
                    printStat("TOP TOTAL ACCIDENTS") {
                        for ((index, pair) in statResults.getTopAccidents(type = AccidentTypeFilter.ALL).withIndex()) {
                            println("${index + 1}. ${pair.first} - ${pair.second}")
                        }
                    }
                } else {
                    when (inputSplit[2]) {
                        "general" -> {
                            printStat("TOP TOTAL GENERAL ACCIDENTS") {
                                for ((index, pair) in statResults.getTopAccidents(type = AccidentTypeFilter.GENERAL_ONLY).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second}")
                                }
                            }
                        }
                        "strength" -> {
                            printStat("TOP TOTAL STRENGTH ACCIDENTS") {
                                for ((index, pair) in statResults.getTopAccidents(type = AccidentTypeFilter.STRENGTH_ONLY).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second}")
                                }
                            }
                        }
                        "flag" -> {
                            printStat("TOP TOTAL FLAG ACCIDENTS") {
                                for ((index, pair) in statResults.getTopAccidents(type = AccidentTypeFilter.FLAG_ONLY).withIndex()) {
                                    println("${index + 1}. ${pair.first} - ${pair.second}")
                                }
                            }
                        }
                        else -> {
                            println("Unknown command '${inputSplit.joinToString(separator = " ")}'")
                        }
                    }
                }
            }

            else -> {
                println("Unknown ${inputSplit?.get(0)} command '${inputSplit?.get(1)}'")
            }
        }
    }
}

private fun printHelp() {
    println("Available commands:")
    println("'exit' - end this program")
    println("'help' - display this list")
    println()
    println("'tplayers' - toggle including players into stats")
    println("'tbots' - toggle including bots into stats")
    println()
    println("'abs' - get absolute value stats")
    println("     'accidents' - Top all accidents count")
    println("          'general' - Top general accidents")
    println("          'strength' - Top accidents with strength buff")
    println("          'flag' - Top accidents while carrying flag")
    println("     'ctf' - Capture the flag stats")
    println("          'captured' - Top flag captures")
    println("          'drops' - Top flag drops")
    println("          'pickups' - Top flag pickups")
    println("          'returns' - Top flag returns")
    println("          'steals' - Top flag steals")
    println("     'dmgGiven' - Top damage given")
    println("     'dmgTaken' - Top damage taken")
    println("     'flagCarrierKills' - Top kills of flag carriers")
    println("     'gamemodePM' - Top gamemode played per map")
    println("     'gamemodePP' - Top gamemode played per player")
    println("     'dmgTaken' - Top damage taken")
    println("     'firstblood' - Top firstblood players")
    println("     'firstbloodVic' - Top firstblood victim players")
    println("     'headshotGive' - Top headshot givers")
    println("     'killingSpree' - Largest killing spree")
    println("     'killers' - Top killers")
    println("     'mapAccidents' - Top accidents per map (absolute)")
    println("     'mapAccidentsPP' - Top accidents per map (per number of players)")
    println("     'mapKills' - Top kills per map (absolute)")
    println("     'mapKillsPP' - Top kills per map (per number of players)")
    println("     'mapSuicides' - Top suicides per map (absolute)")
    println("     'mapSuicidesPP' - Top suicides per map (per number of players)")
    println("     'mapnamePM' - Top maps played (per map)")
    println("     'mapnamePP' - Top maps played (per player)")
    println("     'playTime' - Total play time per player")
    println("     'pvpKills' - Who the players killed the most time")
    println("     'pvpDeaths' - Who the players were killed by the most time")
    println("     'secondaryFireKillers' - Total kills with secondary fire per player")
    println("     'secondaryFireVictims' - Total deaths with secondary fire per player")
    println("     'shotgunSmashers' - Total kills with shotgun's secondary fire")
    println("     'shotgunSmasherDeaths' - Total deaths with shotgun's secondary fire")
    println("     'splash' - Total kills with splash damage")
    println("     'splashD' - Total deaths with splash damage")
    println("     'strength' - Total kills with strength buff")
    println("     'strengthD' - Total deaths with strength buff")
    println("     'strengthKillingSpree' - Largest killing spree with strength buff")
    println("     'typerKillers' - Total kills of typing players")
    println("     'typeDeaths' - Total deaths when typing")
    println("     'victims' - Top deaths count")
    println("     'winnersPM' - Top winners per map (absolute numbers)")
    println("     'weapons' - List of top kills with a certain weapon")
    println("     'weaponsD' - List of top deaths with a certain weapon")
    println()
    println("'spm' - get normalized value stats (usually per minute)")
    println("     'accidents' - Top all accidents count")
    println("          'general' - Top general accidents")
    println("          'strength' - Top accidents with strength buff")
    println("          'flag' - Top accidents while carrying flag")
    println("     'ctf' - Capture the flag stats")
    println("          'captured' - Top flag captures")
    println("          'drops' - Top flag drops")
    println("          'pickups' - Top flag pickups")
    println("          'returns' - Top flag returns")
    println("          'steals' - Top flag steals")
    println("     'dmgGiven' - Top damage given")
    println("     'dmgTaken' - Top damage taken")
    println("     'flagCarrierKills' - Top kills of flag carriers")
    println("     'firstblood' - Top firstblood players")
    println("     'firstbloodVic' - Top firstblood victim players")
    println("     'headshotGive' - Top headshot givers")
    println("     'killers' - Top killers")
    println("     'killsPerMap' - Top killers per maps played")
    println("     'secondaryFireKillers' - Total kills with secondary fire per player")
    println("     'secondaryFireVictims' - Total deaths with secondary fire per player")
    println("     'shotgunSmashers' - Total kills with shotgun's secondary fire")
    println("     'shotgunSmasherDeaths' - Total deaths with shotgun's secondary fire")
    println("     'splash' - Total kills with splash damage")
    println("     'splashD' - Total deaths with splash damage")
    println("     'strength' - Total kills with strength buff")
    println("     'strengthD' - Total deaths with strength buff")
    println("     'typerKillers' - Total kills of typing players")
    println("     'typeDeaths' - Total deaths when typing")
    println("     'victims' - Top deaths count")
    println("     'winnersPM' - Top winners per map per absence (when present, how many times the player wins)")
    println("     'weapons' - List of top kills with a certain weapon")
    println("     'weaponsD' - List of top deaths with a certain weapon")
}

fun analyze(results: List<XonoticMap>): StatResults {
    val sr = StatResults()

    for (map in results) {
        // check if map ever hosted bots - the only way to exclude some bot things that can't be filtered otherwise - like dmg taken or given or playtime
        val isBotMap = map.scores.any { it.nicknameFull.contains("[BOT]") }
        val isReallyBotMap = map.playedWithBots
        
        val isCtf = map.mapGameMode == "ctf"

        val mapStat = sr.maps.getOrPut(map.mapName) { MapStat(map.mapName) }
        // these maps should already be filtered to not be empty (someone must have played it) so no checks needed
        mapStat.addPlayed(map.mapGameMode)
        mapStat.addKills(map.frags.size, map.players.size)
        mapStat.addSuicides(map.suicides.size, map.players.size)
        mapStat.addAccidents(map.accidents.size, map.players.size)
        val mapWinner = map.scores.maxByOrNull { it.score }
        mapWinner?.let {
            val winner = sr.players.getOrPut(it.nicknameFull) { PlayerStat(it.nicknameFull) }
            mapStat.addWinner(winner)
        }

        // process score
        for (score in map.scores) {
            val player = sr.players.getOrPut(score.nicknameFull) { PlayerStat(score.nicknameFull) }

            player.addPlaytime(score.playtime, isBotMap, isCtf)
            player.addDmgGiven(score.dmg.toDouble(), isBotMap)
            player.addDmgTaken(score.dmgtaken.toDouble(), isBotMap)
            player.addGamePlayed(map.mapGameMode, map.mapName)
        }

        // process frags
        var processedFirstblood = false
        for (frag in map.frags.sortedBy { it.time }) {
            val killer = map.players[frag.killerId.toString()]?.nickname ?: ""
            val victim = map.players[frag.victimId.toString()]?.nickname ?: ""

            val killerPlayer = sr.players.getOrPut(killer) { PlayerStat(killer) }
            val victimPlayer = sr.players.getOrPut(victim) { PlayerStat(victim) }

            if (!processedFirstblood && !killerPlayer.isBot && !victimPlayer.isBot) {
                // store first blood
                killerPlayer.addFirstblood(victimPlayer)
                victimPlayer.addFirstbloodVictim(killerPlayer)
                processedFirstblood = true
            }
            
            killerPlayer.addPlayerKilled(victimPlayer, frag.deathType, frag.weapon, frag.victimItemString.flags)
            victimPlayer.addDeath(killerPlayer, frag.deathType, frag.weapon, frag.victimItemString.flags)
        }

        // process accidents
        for (accident in map.accidents) {
            val victim = map.players[accident.playerId.toString()]?.nickname ?: ""

            val victimPlayer = sr.players.getOrPut(victim) { PlayerStat(victim) }

            victimPlayer.addAccident(accident)
        }

        // process ctf events
        for (ctfAction in map.ctfActions) {
            val actor = map.players[ctfAction.stealerId.toString()]?.nickname ?: ""
            val actorPlayer = sr.players.getOrPut(actor) { PlayerStat(actor) }

            actorPlayer.addCtfAction(ctfAction)
        }

        // process killing spree
        var fragCounter = 0
        var accidentCounter = 0
        val tempKillingSpree = mutableMapOf<String, Int>()
        val finalKillingSpree = mutableMapOf<String, Int>()
        val tempStrengthKillingSpree = mutableMapOf<String, Int>()
        val finalStrengthKillingSpree = mutableMapOf<String, Int>()
        while (fragCounter != map.frags.size && accidentCounter != map.accidents.size) {
            val frag = map.frags.getOrNull(fragCounter)
            val accident = map.accidents.getOrNull(accidentCounter)
            if (frag == null && accident == null) {
                // ???
            } else if (frag == null && accident != null) {
                // only accidents remain
                processAccidentSpree(map, accident, tempKillingSpree, finalKillingSpree, tempStrengthKillingSpree, finalStrengthKillingSpree)
                accidentCounter++
            } else if (accident == null && frag != null) {
                // only frags remain
                processFragSpree(map, frag, tempKillingSpree, finalKillingSpree, tempStrengthKillingSpree, finalStrengthKillingSpree)

                fragCounter++
            } else if (frag != null && accident != null) {
                if (frag.time!!.isBefore(accident.time)) {
                    // frag came first

                    processFragSpree(map, frag, tempKillingSpree, finalKillingSpree, tempStrengthKillingSpree, finalStrengthKillingSpree)

                    fragCounter++
                } else {
                    // accident came first

                    processAccidentSpree(map, accident, tempKillingSpree, finalKillingSpree, tempStrengthKillingSpree, finalStrengthKillingSpree)

                    accidentCounter++
                }
            }
        }
        if(tempKillingSpree.isNotEmpty()) {
            // process the rest of the records in temp
            val keys = tempKillingSpree.keys.toList()
            for (name in keys) {
                endSpree(name, tempKillingSpree, finalKillingSpree)
            }
        }
        if(tempStrengthKillingSpree.isNotEmpty()) {
            // process the rest of the records
            val keys = tempStrengthKillingSpree.keys.toList()
            for (name in keys) {
                endStrengthSpree(name, tempStrengthKillingSpree, finalStrengthKillingSpree)
            }
        }
        for ((index, value) in finalKillingSpree) {
            val player = sr.players.getOrPut(index, { PlayerStat(index) })
            player.setKillingSpree(map.mapName, value)
        }
        for((index, value) in finalStrengthKillingSpree) {
            val player = sr.players.getOrPut(index, { PlayerStat(index) })
            player.setStrengthKillingSpree(map.mapName, value)
        }

    }

    sr.players.remove("")

    return sr
}

private fun processAccidentSpree(map: XonoticMap, accident: XonoticAccident, tempKillingSpree: MutableMap<String, Int>, finalKillingSpree: MutableMap<String, Int>, tempStrengthKillingSpree: MutableMap<String, Int>, finalStrengthKillingSpree: MutableMap<String, Int>) {
    val accName = map.players[accident.playerId.toString()]?.nickname ?: ""
    endSpree(accName, tempKillingSpree, finalKillingSpree)

    endStrengthSpree(accName, tempStrengthKillingSpree, finalStrengthKillingSpree)
}

private fun processFragSpree(map: XonoticMap, frag: XonoticFrag, tempKillingSpree: MutableMap<String, Int>, finalKillingSpree: MutableMap<String, Int>, tempStrengthKillingSpree: MutableMap<String, Int>, finalStrengthKillingSpree: MutableMap<String, Int>) {
    val killerName = map.players[frag.killerId.toString()]?.nickname ?: ""
    val victimName = map.players[frag.victimId.toString()]?.nickname ?: ""

    endSpree(victimName, tempKillingSpree, finalKillingSpree)

    // filter killing bots
    if(!victimName.contains("[BOT]")) {
        increaseSpree(tempKillingSpree, killerName)
    }

    if(frag.killerItemString.flags.contains(XonoticItemStringFlag.STRENGTH)) {
        // process spree with strength buff
        endStrengthSpree(victimName, tempStrengthKillingSpree, finalStrengthKillingSpree)

        // filter killing bots
        if(!victimName.contains("[BOT]")) {
            increaseStrengthSpree(killerName, tempStrengthKillingSpree)
        }
    }
}

fun increaseStrengthSpree(killerName: String, tempStrengthKillingSpree: MutableMap<String, Int>) {
    val killerSpree = tempStrengthKillingSpree.getOrDefault(killerName, 0)

    tempStrengthKillingSpree[killerName] = killerSpree + 1
}

fun endStrengthSpree(
    playerName: String,
    tempStrengthKillingSpree: MutableMap<String, Int>,
    finalStrengthKillingSpree: MutableMap<String, Int>
) {
    val playerSpree = tempStrengthKillingSpree.getOrDefault(playerName, 0)

    val finalSpree = finalStrengthKillingSpree.getOrDefault(playerName, -1)

    if (playerSpree > finalSpree) {
        // new record for the player
        finalStrengthKillingSpree[playerName] = playerSpree
        tempStrengthKillingSpree.remove(playerName)
    }
}

private fun increaseSpree(tempKillingSpree: MutableMap<String, Int>, killerName: String) {
    val killerSpree = tempKillingSpree.getOrDefault(killerName, 0)

    tempKillingSpree[killerName] = killerSpree + 1
}

private fun endSpree(playerName: String, tempKillingSpree: MutableMap<String, Int>, finalKillingSpree: MutableMap<String, Int>) {
    val playerSpree = tempKillingSpree.getOrDefault(playerName, 0)

    val finalSpree = finalKillingSpree.getOrDefault(playerName, -1)

    if (playerSpree > finalSpree) {
        // new record for the player
        finalKillingSpree[playerName] = playerSpree
        tempKillingSpree.remove(playerName)
    }
}

fun getEntityFilter(withPlayers: Boolean, withBots: Boolean): EntityTypeFilter {
    return when {
        withPlayers && withBots -> EntityTypeFilter.ALL
        withPlayers && !withBots -> EntityTypeFilter.PLAYERS
        !withPlayers && withBots -> EntityTypeFilter.BOTS
        else -> {
            //fck you, you're getting all
            EntityTypeFilter.ALL
        }
    }
}

inline fun printStat(title: String, statRowsPrintFunc: () -> Any) {
    println("------------------------------------------------------------------")
    println(title)
    statRowsPrintFunc()
    println()
}