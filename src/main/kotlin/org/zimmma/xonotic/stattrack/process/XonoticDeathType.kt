package org.zimmma.xonotic.stattrack.process

/**
 * Enum storing death types in Xonotic.
 *
 * @author Marek Zimmermann
 */
enum class XonoticDeathType(val flagValue: Int) {
    TELEFRAG(-5), FALL(-4), LAVA(-3), ACCIDENT_TRAP(-2), PRIMARY(0), SECONDARY_FIRE(256), SPLASH_DAMAGE(512), BOUNCED_PROJECTILE(1024), HEADSHOT(2048), UNUSED(4096);

    companion object {
        fun getDeathTypes(fragValue: Int): List<XonoticDeathType> {
            val results = mutableListOf<XonoticDeathType>()
            var tmp = fragValue
            while (tmp >= SECONDARY_FIRE.flagValue) {
                loop@ for (deathType in values().reversed()) {
                    if (deathType.flagValue < fragValue) {
                        results.add(deathType)
                        tmp -= deathType.flagValue
                        if(deathType.flagValue < 0) {
                            break@loop
                        }
                    }
                }
            }

            if (results.isEmpty()) {
                results.add(PRIMARY)
            }

            return results
        }
    }
}