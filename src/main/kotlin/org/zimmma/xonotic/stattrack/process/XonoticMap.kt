package org.zimmma.xonotic.stattrack.process

import org.zimmma.xonotic.stattrack.process.ctf.XonoticCtfAction
import org.zimmma.xonotic.stattrack.process.kill.XonoticAccident
import org.zimmma.xonotic.stattrack.process.kill.XonoticFrag
import org.zimmma.xonotic.stattrack.process.kill.XonoticSuicide
import org.zimmma.xonotic.stattrack.process.kill.XonoticTeamKill
import org.zimmma.xonotic.stattrack.process.vote.XonoticVoteCall
import org.zimmma.xonotic.stattrack.process.vote.XonoticVoteResult
import java.time.LocalDateTime

/**
 * Class representing one map played in Xonotic.
 *
 * @author Marek Zimmermann
 */
data class XonoticMap(
        var logVersion: Int = 3,
        var playedWithBots: Boolean = false,
        var mapLoaded: LocalDateTime? = null,
        var mapStarted: LocalDateTime? = null,
        var mapEnded: LocalDateTime? = null,
        var mapGameMode: String = "",
        var mapName: String = "",
        var matchId: String = "",
        val mutators: MutableList<String> = mutableListOf(),

        val players: MutableMap<String, XonoticPlayer> = mutableMapOf(),

        val frags: MutableList<XonoticFrag> = mutableListOf(),
        val teamKills: MutableList<XonoticTeamKill> = mutableListOf(),
        val accidents: MutableList<XonoticAccident> = mutableListOf(),
        val suicides: MutableList<XonoticSuicide> = mutableListOf(),
        val ctfActions: MutableList<XonoticCtfAction> = mutableListOf(),
        val scores: MutableList<XonoticScoreLabel> = mutableListOf(),
        val teamScores: MutableList<XonoticTeamScoreLabel> = mutableListOf(),
        val chatMessages: MutableList<XonoticChatRecord> = mutableListOf(),
        val voteCalls: MutableList<XonoticVoteCall> = mutableListOf(),
        val voteResults: MutableList<XonoticVoteResult> = mutableListOf()
) {
    fun addFrag(frag: XonoticFrag) {
        frags.add(frag)
    }
    
    fun addTeamKill(teamKill: XonoticTeamKill) {
        teamKills.add(teamKill)
    }
    
    fun addScore(score: XonoticScoreLabel) {
        scores.add(score)
    }

    fun addTeamScore(teamScore: XonoticTeamScoreLabel) {
        teamScores.add(teamScore)
    }
    
    fun addAccident(accident: XonoticAccident) {
        accidents.add(accident)
    }

    fun addSuicide(suicide: XonoticSuicide) {
        suicides.add(suicide)
    }

    fun addPlayer(player: XonoticPlayer) {
        players[player.playerId.toString()] = player
        if(player.nickname.startsWith("[BOT]"))
            playedWithBots = true
    }
    
    fun addTeamJoin(team: XonoticTeam) {
        players[team.playerId.toString()]?.team?.add(team)
    }
    
    fun addChatLog(chatRecord: XonoticChatRecord) {
        chatMessages.add(chatRecord)
    }

    fun addCtfAction(ctfAction: XonoticCtfAction) {
        ctfActions.add(ctfAction)
    }
    
    fun addVoteCall(voteCall: XonoticVoteCall) {
        voteCalls.add(voteCall)
    }
    
    fun addVoteResult(voteResult: XonoticVoteResult) {
        voteResults.add(voteResult)
    }

    /**
     * Returns true if map has no significant actions - no players, no frags etc.
     */
    fun mapIsEmpty(): Boolean {
        if(players.isEmpty()) {
            debugPrint("$mapName - no players: $this")
            return true
        }

        when (mapGameMode) {
            "ctf" -> {
                if(ctfActions.isEmpty()) {
                    //debugPrint("$mapName - is CTF but no CTF actions: $this")
                    return true
                }
            }
            else -> {
                if(frags.isEmpty()) {
                    //debugPrint("$mapName - no frags: $this")
                    return true
                }
            }
        }

        return false
    }
    
    fun debugPrint(value: String) {
        if(DEBUG) {
            println(value)
        }
    }

    companion object {
        const val DEBUG = false
    }
}