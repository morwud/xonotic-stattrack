package org.zimmma.xonotic.stattrack.process.vote

import java.time.LocalDateTime

/**
 * Class representing one vote result.
 *
 * @author Marek Zimmermann
 */
data class XonoticVoteResult(
        val succeeded: Boolean = false,
        val yesCount: Int = 0,
        val noCount: Int = 0,
        val abstainCount: Int = 0,
        val notVoters: Int = 0,
        val minCount: Int = 0,
        val time: LocalDateTime? = null
)