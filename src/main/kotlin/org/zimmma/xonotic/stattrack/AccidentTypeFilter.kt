package org.zimmma.xonotic.stattrack

/**
 * Describes how we want to filter accidents by various types.
 */
enum class AccidentTypeFilter {
    ALL, FLAG_ONLY, STRENGTH_ONLY, LAVA_ONLY, FALL_ONLY, GENERAL_ONLY
}