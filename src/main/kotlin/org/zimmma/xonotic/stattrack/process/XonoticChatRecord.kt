package org.zimmma.xonotic.stattrack.process

import java.time.LocalDateTime

/**
 * Class representing one chat line.
 *
 * @author Marek Zimmermann
 */
data class XonoticChatRecord(
        val playerId: Int = 0,
        val team: XonoticTeamType = XonoticTeamType.UNKNOWN,
        val message: String = "",
        val time: LocalDateTime? = null
) {
}