package org.zimmma.xonotic.stattrack.process.vote

/**
 * Represents one flag of itemstring.
 *
 * @author Marek Zimmermann
 */
enum class XonoticItemStringFlag(val value: Char) {
    FLAG_CARRIER('F'), STRENGTH('S'), SHIELD('I'), TYPING('T');

    companion object {
        fun getAllStringFlags(input: String): List<XonoticItemStringFlag> {
            val result = mutableListOf<XonoticItemStringFlag>()

            for (char in input) {
                for (itemStringFlag in values()) {
                    if(char == itemStringFlag.value)
                        result.add(itemStringFlag)
                }
            }
            
            return result
        }
    }
}