package org.zimmma.xonotic.stattrack.process

import java.time.LocalDateTime

/**
 * Class representing one player in Xonotic.
 *
 * @author Marek Zimmermann
 */
data class XonoticPlayer(
        val playerId: Int = 0,
        val slot: Int = 0,
        val ipAddressString: String = "",
        val nickname: String = "",
        val team: MutableList<XonoticTeam> = mutableListOf(),
        val time: LocalDateTime? = null
) {
}