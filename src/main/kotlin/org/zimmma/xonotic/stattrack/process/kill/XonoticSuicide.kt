package org.zimmma.xonotic.stattrack.process.kill

import org.zimmma.xonotic.stattrack.process.XonoticDeathType
import org.zimmma.xonotic.stattrack.process.XonoticItemString
import org.zimmma.xonotic.stattrack.process.XonoticWeapon
import org.zimmma.xonotic.stattrack.process.getItemString
import java.time.LocalDateTime

/**
 * Class representing one suicide event on a map.
 *
 * @author Marek Zimmermann
 */
data class XonoticSuicide(
        val playerId: Int = 0,
        val deathType: List<XonoticDeathType> = listOf(XonoticDeathType.PRIMARY),
        val weapon: XonoticWeapon = XonoticWeapon.UNKNOWN,
        val victimItemString: XonoticItemString = XonoticItemString(),
        val time: LocalDateTime? = null
) {
    constructor(playerId: Int, fragValue: Int, victimItemString: String) : this(
            playerId = playerId,
            deathType = XonoticDeathType.getDeathTypes(fragValue),
            weapon = XonoticWeapon.getWeaponType(fragValue - XonoticDeathType.getDeathTypes(fragValue).sumBy { it.flagValue }),
            victimItemString = getItemString(victimItemString)
    )
}