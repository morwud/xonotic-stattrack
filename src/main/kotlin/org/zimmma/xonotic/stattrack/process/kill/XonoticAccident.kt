package org.zimmma.xonotic.stattrack.process.kill

import org.zimmma.xonotic.stattrack.process.XonoticDeathType
import org.zimmma.xonotic.stattrack.process.XonoticItemString
import java.time.LocalDateTime

/**
 * Class representing one accident event on a map.
 *
 * @author Marek Zimmermann
 */
data class XonoticAccident(
        val playerId: Int = 0,
        val deathType: XonoticDeathType = XonoticDeathType.ACCIDENT_TRAP,
        val victimItemString: XonoticItemString = XonoticItemString(),
        val time: LocalDateTime? = null
) {
}