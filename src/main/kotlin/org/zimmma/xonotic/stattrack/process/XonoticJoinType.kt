package org.zimmma.xonotic.stattrack.process

/**
 * Enum represents connection type.
 *
 * @author Marek Zimmermann
 */
enum class XonoticJoinType(val value: Int) {
    UNKNOWN(-1), CONNECT(1), AUTO(2), MANUAL(3), SPECTATING(4), ADMIN_MOVE(6);

    companion object {
        fun getJoinType(value: Int): XonoticJoinType {
            for (joinType in values().reversed()) {
                if(value == joinType.value)
                    return joinType
            }
            
            throw IllegalArgumentException("Unknown type value '$value'")
        }
    }
}