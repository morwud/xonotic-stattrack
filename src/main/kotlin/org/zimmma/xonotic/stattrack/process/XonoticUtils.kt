package org.zimmma.xonotic.stattrack.process

import org.zimmma.xonotic.stattrack.process.vote.XonoticItemStringFlag

/**
 * Util functions.
 *
 * @author Marek Zimmermann
 */

val _hex_col_regex = "\\^x([\\dA-Fa-f])([\\dA-Fa-f])([\\dA-Fa-f])"
val _dec_col_regex = "\\^(\\d)"
val _all_col_regex = Regex("\\^(\\d|x[\\dA-Fa-f]{3})")

val _qfont_ascii_table = listOf(
    "\\0", "#",  "#",  "#",  "#",  ".",  "#",  "#",
    "#",  "\t", "\n", "#",  " ",  "\r", ".",  ".",
    "[",  "]",  "0",  "1",  "2",  "3",  "4",  "5",
    "6",  "7",  "8",  "9",  ".",  "<",  "=",  ">",
    " ",  "!",  "\"",  "#",  "$",  "%",  "&",  "\"",
    "(",  ")",  "*",  "+",  ",",  "-",  ".",  "/",
    "0",  "1",  "2",  "3",  "4",  "5",  "6",  "7",
    "8",  "9",  ":",  ";",  "<",  "=",  ">",  "?",
    "@",  "A",  "B",  "C",  "D",  "E",  "F",  "G",
    "H",  "I",  "J",  "K",  "L",  "M",  "N",  "O",
    "P",  "Q",  "R",  "S",  "T",  "U",  "V",  "W",
    "X",  "Y",  "Z",  "[",  "\\", "]",  "^",  "_",
    "`",  "a",  "b",  "c",  "d",  "e",  "f",  "g",
    "h",  "i",  "j",  "k",  "l",  "m",  "n",  "o",
    "p",  "q",  "r",  "s",  "t",  "u",  "v",  "w",
    "x",  "y",  "z",  "{",  "|",  "}",  "~",  "<",

    "<",  "=",  ">",  "#",  "#",  ".",  "#",  "#",
    "#",  "#",  " ",  "#",  " ",  ">",  ".",  ".",
    "[",  "]",  "0",  "1",  "2",  "3",  "4",  "5",
    "6",  "7",  "8",  "9",  ".",  "<",  "=",  ">",
    " ",  "!",  "\"",  "#",  "$",  "%",  "&",  "\"",
    "(",  ")",  "*",  "+",  ",",  "-",  ".",  "/",
    "0",  "1",  "2",  "3",  "4",  "5",  "6",  "7",
    "8",  "9",  ":",  ";",  "<",  "=",  ">",  "?",
    "@",  "A",  "B",  "C",  "D",  "E",  "F",  "G",
    "H",  "I",  "J",  "K",  "L",  "M",  "N",  "O",
    "P",  "Q",  "R",  "S",  "T",  "U",  "V",  "W",
    "X",  "Y",  "Z",  "[",  "\\", "]",  "^",  "_",
    "`",  "a",  "b",  "c",  "d",  "e",  "f",  "g",
    "h",  "i",  "j",  "k",  "l",  "m",  "n",  "o",
    "p",  "q",  "r",  "s",  "t",  "u",  "v",  "w",
    "x",  "y",  "z",  "{",  "|",  "}",  "~",  "<"
)

val _qfont_unicode_glyphs = listOf(
        "\u0020", "\u0020", "\u2014", "\u0020",
        "\u005F", "\u2747", "\u2020", "\u00B7",
        "\uD83D\uDD2B", "\u0020", "\u0020", "\u25A0",
        "\u2022", "\u2192", "\u2748", "\u2748",
        "\u005B", "\u005D", "\uD83D\uDC7D", "\uD83D\uDE03",
        "\uD83D\uDE1E", "\uD83D\uDE35", "\uD83D\uDE15", "\uD83D\uDE0A",
        "\u00AB", "\u00BB", "\u2022", "\u203E",
        "\u2748", "\u25AC", "\u25AC", "\u25AC",
        "\u0020", "\u0021", "\u0022", "\u0023",
        "\u0024", "\u0025", "\u0026", "\u0027",
        "\u0028", "\u0029", "\u00D7", "\u002B",
        "\u002C", "\u002D", "\u002E", "\u002F",
        "\u0030", "\u0031", "\u0032", "\u0033",
        "\u0034", "\u0035", "\u0036", "\u0037",
        "\u0038", "\u0039", "\u003A", "\u003B",
        "\u003C", "\u003D", "\u003E", "\u003F",
        "\u0040", "\u0041", "\u0042", "\u0043",
        "\u0044", "\u0045", "\u0046", "\u0047",
        "\u0048", "\u0049", "\u004A", "\u004B",
        "\u004C", "\u004D", "\u004E", "\u004F",
        "\u0050", "\u0051", "\u0052", "\u0053",
        "\u0054", "\u0055", "\u0056", "\u0057",
        "\u0058", "\u0059", "\u005A", "\u005B",
        "\u005C", "\u005D", "\u005E", "\u005F",
        "\u0027", "\u0061", "\u0062", "\u0063",
        "\u0064", "\u0065", "\u0066", "\u0067",
        "\u0068", "\u0069", "\u006A", "\u006B",
        "\u006C", "\u006D", "\u006E", "\u006F",
        "\u0070", "\u0071", "\u0072", "\u0073",
        "\u0074", "\u0075", "\u0076", "\u0077",
        "\u0078", "\u0079", "\u007A", "\u007B",
        "\u007C", "\u007D", "\u007E", "\u2190",
        "\u003C", "\u003D", "\u003E", "\uD83D\uDE80",
        "\u00A1", "\u004F", "\u0055", "\u0049",
        "\u0043", "\u00A9", "\u00AE", "\u25A0",
        "\u00BF", "\u25B6", "\u2748", "\u2748",
        "\u2772", "\u2773", "\uD83D\uDC7D", "\uD83D\uDE03",
        "\uD83D\uDE1E", "\uD83D\uDE35", "\uD83D\uDE15", "\uD83D\uDE0A",
        "\u00AB", "\u00BB", "\u2747", "\u0078",
        "\u2748", "\u2014", "\u2014", "\u2014",
        "\u0020", "\u0021", "\u0022", "\u0023",
        "\u0024", "\u0025", "\u0026", "\u0027",
        "\u0028", "\u0029", "\u002A", "\u002B",
        "\u002C", "\u002D", "\u002E", "\u002F",
        "\u0030", "\u0031", "\u0032", "\u0033",
        "\u0034", "\u0035", "\u0036", "\u0037",
        "\u0038", "\u0039", "\u003A", "\u003B",
        "\u003C", "\u003D", "\u003E", "\u003F",
        "\u0040", "\u0041", "\u0042", "\u0043",
        "\u0044", "\u0045", "\u0046", "\u0047",
        "\u0048", "\u0049", "\u004A", "\u004B",
        "\u004C", "\u004D", "\u004E", "\u004F",
        "\u0050", "\u0051", "\u0052", "\u0053",
        "\u0054", "\u0055", "\u0056", "\u0057",
        "\u0058", "\u0059", "\u005A", "\u005B",
        "\u005C", "\u005D", "\u005E", "\u005F",
        "\u0027", "\u0041", "\u0042", "\u0043",
        "\u0044", "\u0045", "\u0046", "\u0047",
        "\u0048", "\u0049", "\u004A", "\u004B",
        "\u004C", "\u004D", "\u004E", "\u004F",
        "\u0050", "\u0051", "\u0052", "\u0053",
        "\u0054", "\u0055", "\u0056", "\u0057",
        "\u0058", "\u0059", "\u005A", "\u007B",
        "\u007C", "\u007D", "\u007E", "\u25C0"
)

fun qfontToString(qstr: String = "", glyphTranslation: Boolean = false): String {
    val chars = mutableListOf<String>()
    for (c in qstr) {
        var char: String = c.toString()
        val cInt = c.toInt()
        if(c in '\ue000'..'\ue0ff') {
            char = if(glyphTranslation) {
                _qfont_unicode_glyphs[c.toInt() - 0xe000]
            } else {
                _qfont_ascii_table[c.toInt() - 0xe000]
            }
        }
        chars.add(char)
    }
    return chars.joinToString(separator = "").replace(_all_col_regex, "")
}

fun getItemString(value: String): XonoticItemString {
    val split = value.split("-")
    /*try {
        val tmp = split[0].toInt()
    } catch (e: NumberFormatException) {
        println("NPE: $value")
    }*/
    // ifs are correcting value equal to "-1-1"
    return XonoticItemString(XonoticWeapon.getWeaponType(if (value.startsWith("-1")) -1 else split[0].toInt()), XonoticItemStringFlag.getAllStringFlags(if (value.startsWith("-1")) split[2] else split[1]))
}