package org.zimmma.xonotic.stattrack.process.vote

import java.time.LocalDateTime

/**
 * Class representing one vote call.
 *
 * @author Marek Zimmermann
 */
data class XonoticVoteCall(
        val playerId: Int = 0,
        val call: String = "",
        val time: LocalDateTime? = null
) {
}