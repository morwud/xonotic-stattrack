package org.zimmma.xonotic.stattrack.process

import org.zimmma.xonotic.stattrack.process.ctf.XonoticCtfAction
import org.zimmma.xonotic.stattrack.process.ctf.XonoticCtfActionType
import org.zimmma.xonotic.stattrack.process.kill.XonoticAccident
import org.zimmma.xonotic.stattrack.process.kill.XonoticFrag
import org.zimmma.xonotic.stattrack.process.kill.XonoticSuicide
import org.zimmma.xonotic.stattrack.process.kill.XonoticTeamKill
import org.zimmma.xonotic.stattrack.process.vote.XonoticVoteCall
import org.zimmma.xonotic.stattrack.process.vote.XonoticVoteResult
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Utill class containing the main function to process the game logs.
 *
 * @author Marek Zimmermann
 */

val datetimePattern: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

fun processLogs(dataFolder: File, eventsPrefix: String): List<XonoticMap> {
    val events = mutableListOf<XonoticMap>()
    // analyze events
    val filesList = dataFolder.listFiles()?.sorted()
    filesList?.forEach { file ->
        if (!file.name.startsWith(eventsPrefix))
            return@forEach

        var lastLineType = ""
        var lastLineSubtype = ""

        var map = XonoticMap()
        var tmpFrag = XonoticFrag()
        var tmpTeamKill = XonoticTeamKill()
        var tmpAccident = XonoticAccident()
        var tmpSuicide = XonoticSuicide()
        var tmpCtfAction = XonoticCtfAction()
        var tmpPlayer = XonoticPlayer()
        var tmpTeam = XonoticTeam()
        var tmpChatMessage = XonoticChatRecord()
        var tmpVoteCall = XonoticVoteCall()
        var tmpVoteResult = XonoticVoteResult()
        var scoreLabels: MutableMap<Int, String> = mutableMapOf()
        if (file.name.startsWith(eventsPrefix)) {
            // events file we want to analyze
            file.forEachLine { line ->
                val lineSplitted = line.split(":")

                lastLineType = if (lineSplitted[1] == "time") lastLineType else lineSplitted[1]
                lastLineSubtype = if (lineSplitted[1] == "time") lastLineSubtype else ""
                when (lineSplitted[1]) {
                    "logversion" -> map.logVersion = lineSplitted[2].toInt()
                    "time" -> {

                        when (lastLineType) {
                            "logversion", "gamestart" -> map.mapLoaded = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern)
                            "gameinfo" -> {
                                if (lastLineSubtype == "end") {
                                    map.mapStarted = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern)
                                }
                            }
                            "startdelay_ended" -> {
                                // probably time limit ended (after that it is sudden death)
                            }
                            "scores" -> map.mapEnded = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern)
                            "kill" ->
                                when (lastLineSubtype) {
                                    "frag" -> {
                                        tmpFrag = tmpFrag.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))
                                        // we have all that's needed for storing one frag
                                        map.addFrag(tmpFrag)
                                        tmpFrag = XonoticFrag()
                                    }
                                    "tk" -> {
                                        tmpTeamKill = tmpTeamKill.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))
                                        map.addTeamKill(tmpTeamKill)
                                        tmpTeamKill = XonoticTeamKill()
                                    }
                                    "accident" -> {
                                        tmpAccident = tmpAccident.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))

                                        map.addAccident(tmpAccident)
                                        tmpAccident = XonoticAccident()
                                    }
                                    "suicide" -> {
                                        tmpSuicide = tmpSuicide.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))

                                        map.addSuicide(tmpSuicide)
                                        tmpSuicide = XonoticSuicide()
                                    }
                                    else -> throw IllegalArgumentException("Not implemented kill time for: $lastLineSubtype")
                                }
                            "join" -> {
                                tmpPlayer = tmpPlayer.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))
                                map.addPlayer(tmpPlayer)
                                tmpPlayer = XonoticPlayer()
                            }
                            "team" -> {
                                tmpTeam = tmpTeam.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))
                                map.addTeamJoin(tmpTeam)
                                tmpTeam = XonoticTeam()
                            }
                            "chat", "chat_team", "chat_spec" -> {
                                tmpChatMessage = tmpChatMessage.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))
                                map.addChatLog(tmpChatMessage)
                                tmpChatMessage = XonoticChatRecord()
                            }
                            "ctf" -> {
                                tmpCtfAction = tmpCtfAction.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))
                                map.addCtfAction(tmpCtfAction)
                                tmpCtfAction = XonoticCtfAction()
                            }
                            "vote" -> {
                                when (lastLineSubtype) {
                                    "vcall" -> {
                                        tmpVoteCall = tmpVoteCall.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))
                                        map.addVoteCall(tmpVoteCall)
                                        tmpVoteCall = XonoticVoteCall()
                                    }

                                    "vyes", "vno" -> {
                                        tmpVoteResult = tmpVoteResult.copy(time = LocalDateTime.parse("${lineSplitted[2]}:${lineSplitted[3]}:${lineSplitted[4]}", datetimePattern))
                                        map.addVoteResult(tmpVoteResult)
                                        tmpVoteResult = XonoticVoteResult()
                                    }
                                }
                            }
                            "labels", "end", "recordset", "player", "teamscores", "part", "anticheat" -> {
                                //all unused line types - no need for their time
                            }
                            else -> {
                                throw IllegalArgumentException("Unknown last line type '$lastLineType' - don't know where to store the time, file '${file.name}'")
                            }
                        }
                    }
                    "gamestart" -> {
                        //map started - reset the object first
                        map = XonoticMap()
                        val mapSplitted = lineSplitted[2].split("_")
                        map.mapGameMode = mapSplitted[0]
                        map.mapName = mapSplitted[1]
                        map.matchId = lineSplitted[3]
                    }
                    "gameinfo" -> {
                        lastLineSubtype = lineSplitted[2]
                        if (lineSplitted[2] == "mutators" && lineSplitted.size > 4) {
                            for (i in 4 until lineSplitted.size) {
                                map.mutators.add(lineSplitted[i])
                            }
                        }

                        if (lineSplitted[2] == "end") {
                            // not sure...
                        }
                    }
                    "startdelay_ended" -> {
                        //nothing
                    }
                    "anticheat" -> {
                        //???
                    }
                    "join" -> {
                        tmpPlayer = XonoticPlayer(lineSplitted[2].toInt(), lineSplitted[3].toInt(), lineSplitted[4], processNickname(lineSplitted[5], true))
                    }
                    "kill" -> {
                        lastLineSubtype = lineSplitted[2]
                        when (lastLineSubtype) {
                            "frag" -> {
                                val killerId = lineSplitted[3].toInt()
                                val victimId = lineSplitted[4].toInt()
                                val killerItems = lineSplitted[6].split("=")[1]
                                val victimItems = lineSplitted[7].split("=")[1]
                                tmpFrag = when (lineSplitted[5]) {
                                    "type=HURTTRIGGER" -> {

                                        XonoticFrag(killerId, victimId, listOf(XonoticDeathType.ACCIDENT_TRAP), XonoticWeapon.UNKNOWN, getItemString(killerItems), getItemString(victimItems))
                                    }
                                    "type=LAVA" -> {
                                        XonoticFrag(killerId, victimId, listOf(XonoticDeathType.LAVA), XonoticWeapon.UNKNOWN, getItemString(killerItems), getItemString(victimItems))
                                    }
                                    "type=FALL" -> {
                                        XonoticFrag(killerId, victimId, listOf(XonoticDeathType.FALL), XonoticWeapon.UNKNOWN, getItemString(killerItems), getItemString(victimItems))
                                    }
                                    "type=TELEFRAG" -> {
                                        XonoticFrag(killerId, victimId, listOf(XonoticDeathType.FALL), XonoticWeapon.UNKNOWN, getItemString(killerItems), getItemString(victimItems))
                                    }
                                    else -> {
                                        val fragValue = lineSplitted[5].split("=")[1].toInt()
                                        XonoticFrag(killerId, victimId, fragValue, killerItems, victimItems)
                                    }
                                }
                            }
                            "tk" -> {
                                val killerId = lineSplitted[3].toInt()
                                val victimId = lineSplitted[4].toInt()
                                val killerItems = lineSplitted[6].split("=")[1]
                                val victimItems = lineSplitted[7].split("=")[1]
                                tmpTeamKill = if (lineSplitted[5] == "type=HURTTRIGGER") {
                                    XonoticTeamKill(killerId, victimId, listOf(XonoticDeathType.ACCIDENT_TRAP), XonoticWeapon.UNKNOWN, getItemString(killerItems), getItemString(victimItems))
                                } else {
                                    val fragValue = lineSplitted[5].split("=")[1].toInt()
                                    XonoticTeamKill(killerId, victimId, fragValue, killerItems, victimItems)
                                }
                            }
                            "suicide" -> {
                                val victimId = lineSplitted[3].toInt()
                                val victimItems = lineSplitted[6].split("=")[1]
                                tmpSuicide = if (lineSplitted[5] == "type=HURTTRIGGER") {
                                    XonoticSuicide(victimId, listOf(XonoticDeathType.ACCIDENT_TRAP), XonoticWeapon.UNKNOWN, getItemString(victimItems))
                                } else {
                                    val fragValue = lineSplitted[5].split("=")[1].toInt()
                                    XonoticSuicide(victimId, fragValue, victimItems)
                                }
                            }
                            "accident" -> {
                                val victimId = lineSplitted[3].toInt()
                                val victimItems = lineSplitted[6].split("=")[1]
                                tmpAccident = XonoticAccident(victimId, victimItemString = getItemString(victimItems))
                            }
                        }
                    }
                    "scores" -> {
                        //game type, map name, map runtime (not really reliable) - no need for any of this
                    }
                    "labels" -> {
                        scoreLabels.clear()
                        for ((index, value) in lineSplitted[3].split(",").withIndex()) {
                            /*
                             * Label flags
                             * !! = primary sorting key
                             * <!! = primary sorting key, lower is better
                             * ! = secondary sorting key
                             * <! = secondary sorting key, lower is better
                             * < = lower is better
                             */
                            scoreLabels[index] = value.removeSuffix("!!").removeSuffix("!").removeSuffix("<")
                        }
                    }
                    "player" -> {
                        val row = XonoticScoreLabel()
                        for ((index, value) in lineSplitted[3].split(",").withIndex()) {
                            when (scoreLabels[index]) {
                                "score" -> row.score = value.toInt()
                                "caps" -> row.caps = value.toInt()
                                "captime" -> row.captime = value.toInt()
                                "drops" -> row.drops = value.toInt()
                                "fckills" -> row.fckills = value.toInt()
                                "pickups" -> row.pickups = value.toInt()
                                "returns" -> row.returns = value.toInt()
                                "deaths" -> row.deaths = value.toInt()
                                "dmg" -> row.dmg = value.toFloat()
                                "dmgtaken" -> row.dmgtaken = value.toFloat()
                                "elo" -> row.elo = value.toFloat()
                                "fps" -> row.fps = value.toInt()
                                "kills" -> row.kills = value.toInt()
                                "suicides" -> row.suicides = value.toInt()
                                "teamkills" -> row.teamkills = value.toInt()
                                null -> println("Null on index '$index' with value'$value'")
                                "" -> {
                                    // this can happen for empty columns - ignore it
                                }
                                else -> throw IllegalArgumentException("Unknown score value '${scoreLabels[index]}'")
                            }
                        }
                        row.playtime = lineSplitted[4].toInt()
                        row.team = lineSplitted[5]
                        row.playerId = lineSplitted[6].toInt()
                        row.nicknameTextOnly = processNickname(lineSplitted[7])
                        row.nicknameFull = processNickname(lineSplitted[7], true)

                        map.addScore(row)
                    }
                    "teamscores" -> {
                        val row = XonoticTeamScoreLabel()
                        for ((index, value) in lineSplitted[3].split(",").withIndex()) {
                            when (scoreLabels[index]) {
                                "caps" -> row.caps = if (value == "") 0 else value.toInt()
                                "score" -> row.score = if (value == "") 0 else value.toInt()
                                "" -> {
                                    //yep, can happen - ignore
                                }
                                else -> throw IllegalArgumentException("Unknown team score value '${scoreLabels[index]}'")
                            }
                        }

                        row.teamId = lineSplitted[4].toInt()
                        map.addTeamScore(row)
                    }
                    "team" -> {
                        tmpTeam = XonoticTeam(lineSplitted[2].toInt(), XonoticTeamType.getTeamType(lineSplitted[3].toInt()), XonoticJoinType.getJoinType(lineSplitted[4].toInt()))
                    }
                    "chat" -> {
                        tmpChatMessage = XonoticChatRecord(lineSplitted[2].toInt(), XonoticTeamType.NO_TEAM, lineSplitted.subList(3, lineSplitted.size).joinToString(separator = ":"))
                    }
                    "chat_team" -> {
                        tmpChatMessage = XonoticChatRecord(lineSplitted[2].toInt(), XonoticTeamType.getTeamType(lineSplitted[3].toInt()), lineSplitted.subList(4, lineSplitted.size).joinToString(separator = ":"))
                    }
                    "chat_spec" -> {
                        // spectator chat
                        tmpChatMessage = XonoticChatRecord(lineSplitted[2].toInt(), XonoticTeamType.UNKNOWN, lineSplitted.subList(3, lineSplitted.size).joinToString(separator = ":"))
                    }
                    "ctf" -> {
                        val actionType = XonoticCtfActionType.getCtfActionType(lineSplitted[2])
                        tmpCtfAction = if (actionType == XonoticCtfActionType.RETURNED) {
                            XonoticCtfAction(actionType, flagColor = XonoticTeamType.getTeamType(lineSplitted[3].toInt()))
                        } else {
                            XonoticCtfAction(actionType, XonoticTeamType.getTeamType(lineSplitted[3].toInt()), XonoticTeamType.getTeamType(lineSplitted[4].toInt()), lineSplitted[5].toInt())
                        }
                    }
                    "vote" -> {
                        when (lineSplitted[2]) {
                            "vcall" -> tmpVoteCall = XonoticVoteCall(lineSplitted[3].toInt(), lineSplitted[4])
                            "vyes" -> tmpVoteResult = XonoticVoteResult(true, lineSplitted[3].toInt(), lineSplitted[4].toInt(), lineSplitted[5].toInt(), lineSplitted[6].toInt(), lineSplitted[7].toInt())
                            "vno" -> tmpVoteResult = XonoticVoteResult(false, lineSplitted[3].toInt(), lineSplitted[4].toInt(), lineSplitted[5].toInt(), lineSplitted[6].toInt(), lineSplitted[7].toInt())
                        }
                    }
                    "recordset" -> {
                        //:recordset:<ID of player>:<time in seconds>
                        //???
                    }
                    "part" -> {
                        //???
                    }

                    "gameover" -> {
                        // reset map based temp vars
                        events.add(map)
                        scoreLabels = mutableMapOf()
                    }
                    "end" -> {
                        //prob nothing
                    }
                    else -> throw NotImplementedError("Missing '${lineSplitted[1]}' value parser in line splitter in file '${file.name}' in line '$line'")
                }
            }
        } else {
            return@forEach
        }
    }

    println("Processed events logs, got ${events.size} results")
    //filter empty events & return
    return events.filter { !it.mapIsEmpty() }
}

fun processNickname(value: String, glyphTranslation: Boolean = false): String {
    return qfontToString(value, glyphTranslation)
}