package org.zimmma.xonotic.stattrack.process

import org.zimmma.xonotic.stattrack.process.vote.XonoticItemStringFlag

/**
 * Represents one item string.
 *
 * @author Marek Zimmermann
 */
data class XonoticItemString(
        val weapon: XonoticWeapon = XonoticWeapon.UNKNOWN,
        val flags: List<XonoticItemStringFlag> = emptyList()
) {
}