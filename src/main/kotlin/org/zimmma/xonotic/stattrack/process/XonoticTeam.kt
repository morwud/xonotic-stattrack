package org.zimmma.xonotic.stattrack.process

import java.time.LocalDateTime

/**
 * Represents one team join action.
 *
 * @author Marek Zimmermann
 */
data class XonoticTeam(
        val playerId: Int = 0,
        val teamId: XonoticTeamType = XonoticTeamType.UNKNOWN,
        val joinType: XonoticJoinType = XonoticJoinType.UNKNOWN,
        val time: LocalDateTime? = null
) {
}