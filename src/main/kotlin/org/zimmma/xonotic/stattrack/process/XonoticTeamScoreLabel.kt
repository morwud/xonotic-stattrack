package org.zimmma.xonotic.stattrack.process

/**
 * Class representing one team score row.
 *
 * @author Marek Zimmermann
 */
data class XonoticTeamScoreLabel(
        var score: Int = 0,
        var caps: Int = 0,
        var teamId: Int = 0
)